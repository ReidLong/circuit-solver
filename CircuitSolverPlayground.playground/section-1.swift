// Playground - noun: a place where people can play

import UIKit

var dict1 = []
var dict2 = []



dict1 == dict2

func ||(lhs: Double, rhs: Double) -> Double {
    return lhs * rhs / (lhs + rhs)
}

let resistance = 5 || ( 10 + ((20 || 30) || (20 + 31 + ( 8 || (4 + (10 || 8 || 6))))))




let listA = [1, 2, 3]
let listB = [2, 3, 4]

var listC: [Int] = []
listC += listA

listC

let listD = [CGPointMake(0, 0), CGPointMake(5, 5)]
var listE = [CGPointMake(4, 4), CGPointMake(3, 7)]
listE += listD

listE

var array = [1, 2, 3, 4]

for (index, item) in enumerate(array) {
    if item % 2 == 0 {
        //array.removeAtIndex(index)
    }
}

array

let test = 3 ^ 4 ^ 3

let testName = "A"
let regex = "^([A-Z])(\\1)*$"

extension String {
    func matches(regex: String) -> Bool {
        if let matchRange = self.rangeOfString(regex, options: NSStringCompareOptions.RegularExpressionSearch, range: nil, locale: nil) {
            return true
        }
        return false
    }
}

testName.matches(regex)
"AA".matches(regex)
"AB".matches(regex)
"Z".matches(regex)
"AAB".matches(regex)

func generateNames(depth: Int) -> [String] {
    let startingCharacter: UnicodeScalar = "A"
    var list: [String] = [String](count: 1, repeatedValue: "")
    for round in 0...depth {
    
        var tempList: [String] = []
        for prefix in list {
            for offset in 0..<26 {
                let character = Character(UnicodeScalar(startingCharacter.value + offset))
                tempList.append("\(prefix)\(character)")
            }
        }
        list = tempList
    }
    
    return list
}

func seedList() -> [String] {
    let startingCharacter: UnicodeScalar = "A"
    var list: [String] = []
    for offset in 0..<26 {
        let character = Character(UnicodeScalar(startingCharacter.value + offset))
        list.append("\(character)")
    }
    return list

}


let depth0Names = generateNames(0)
let depth1Names = generateNames(1)
//generateNames(2)

depth0Names.count

