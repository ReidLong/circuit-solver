//
//  CircuitLibrary.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/1/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import UIKit
import XCTest

struct CircuitLibrary {
    static func createCircuitA() -> Circuit {
        var circuit = Circuit()
        let nodeA = Node()
        let nodeB = Node()
        let nodeC = Node()
        let nodeD = Node()
        let resistor1 = Resistor(positiveTerminal: nodeA, negativeTerminal: nodeB, resistance: 10)
        let resistor2 = Resistor(positiveTerminal: nodeB, negativeTerminal: nodeC, resistance: 20)
        let resistor3 = Resistor(positiveTerminal: nodeB, negativeTerminal: nodeD, resistance: 30)
        let resistor4 = Resistor(positiveTerminal: nodeD, negativeTerminal: nodeC, resistance: 40)
        let resistor5 = Resistor(positiveTerminal: nodeD, negativeTerminal: nodeC, resistance: 50)
        let voltageSourceX = VoltageSource(positiveTerminal: nodeA, negativeTerminal: nodeC)
        let voltageSourceY = VoltageSource(positiveTerminal: nodeB, negativeTerminal: nodeC)
        circuit += [resistor1, resistor2, resistor3, resistor4, resistor5]
        circuit += [voltageSourceX, voltageSourceY]
        assertIsValidCircuit(circuit)
        return circuit
    }
    
    static func createCircuitB() -> Circuit {
        var circuit = Circuit()
        let nodeA = Node(name: "A")
        let nodeB = Node(name: "B")
        let nodeC = Node(name: "C")
        println("Node Equality \(nodeA == nodeB)")
        let resistor1 = Resistor(positiveTerminal: nodeA, negativeTerminal: nodeB, resistance: 10)
        resistor1.name = "R1"
        let voltageSourceX = VoltageSource(positiveTerminal: nodeA, negativeTerminal: nodeC)
        voltageSourceX.name = "Vx"
        circuit += [resistor1, voltageSourceX]
        println("Node Contains \(circuit.nodes.contains(nodeA))")
        assertIsValidCircuit(circuit)
        return circuit
    }
    
    static func createCircuitC() -> (Circuit, [Node]) {
        let nodeA = Node(name: "A")
        let nodeB = Node(name: "B")
        let nodeC = Node(name: "C")
        let resistor1 = Resistor(positiveTerminal: nodeA, negativeTerminal: nodeB, resistance: 4)
        resistor1.name = "R1"
        let resistor2 = Resistor(positiveTerminal: nodeB, negativeTerminal: nodeC, resistance: 6)
        resistor2.name = "R2"
        var circuit = Circuit()
        circuit += [resistor1, resistor2]
        assertIsValidCircuit(circuit)
        return (circuit, [nodeA, nodeB, nodeC])
    }
    
    static func createCircuitD() -> (Circuit, [Node]) {
        let nodeA = Node(name: "A")
        let nodeB = Node(name: "B")
        let nodeC = Node(name: "C")
        let nodeD = Node(name: "D")
        let resistor1 = Resistor(positiveTerminal: nodeA, negativeTerminal: nodeB, resistance: 4)
        resistor1.name = "R1"
        let resistor2 = Resistor(positiveTerminal: nodeA, negativeTerminal: nodeB, resistance: 6)
        resistor2.name = "R2"
        let resistor3 = Resistor(positiveTerminal: nodeB, negativeTerminal: nodeC, resistance: 10)
        resistor3.name = "R3"
        let resistor4 = Resistor(positiveTerminal: nodeC, negativeTerminal: nodeD, resistance: 8)
        resistor4.name = "R4"
        let resistor5 = Resistor(positiveTerminal: nodeC, negativeTerminal: nodeD, resistance: 12)
        resistor5.name = "R5"
        var circuit = Circuit()
        circuit += [resistor1, resistor2, resistor3, resistor4, resistor5]
        assertIsValidCircuit(circuit)
        
        return (circuit, [nodeA, nodeB, nodeC, nodeD])
    }
    
    static func createCircuitE() -> (circuitE: Circuit, nodes: [Node]) {
        let nodeA = Node(name: "A")
        let nodeB = Node(name: "B")
        let nodeC = Node(name: "C")
        let nodeD = Node(name: "D")
        let nodeE = Node(name: "E")
        let nodeF = Node(name: "F")
        let nodeG = Node(name: "G")
        let nodeH = Node(name: "H")
        let resistor1 = Resistor(positiveTerminal: nodeA, negativeTerminal: nodeB, resistance: 5, name: "R1")
        let resistor2 = Resistor(positiveTerminal: nodeA, negativeTerminal: nodeC, resistance: 10, name: "R2")
        let resistor3 = Resistor(positiveTerminal: nodeC, negativeTerminal: nodeB, resistance: 20, name: "R3")
        let resistor4 = Resistor(positiveTerminal: nodeC, negativeTerminal: nodeB, resistance: 30, name: "R4")
        let resistor5 = Resistor(positiveTerminal: nodeB, negativeTerminal: nodeH, resistance: 25, name: "R5")
        let resistor6 = Resistor(positiveTerminal: nodeH, negativeTerminal: nodeG, resistance: 6, name: "R6")
        let resistor7 = Resistor(positiveTerminal: nodeC, negativeTerminal: nodeD, resistance: 20, name: "R7")
        let resistor8 = Resistor(positiveTerminal: nodeF, negativeTerminal: nodeE, resistance: 7, name: "R8")
        let resistor9 = Resistor(positiveTerminal: nodeE, negativeTerminal: nodeD, resistance: 3, name: "R9")
        let resistor10 = Resistor(positiveTerminal: nodeF, negativeTerminal: nodeD, resistance: 8, name: "R10")
        let resistor11 = Resistor(positiveTerminal: nodeF, negativeTerminal: nodeD, resistance: 6, name: "R11")
        let resistor12 = Resistor(positiveTerminal: nodeF, negativeTerminal: nodeG, resistance: 4, name: "R12")
        let resistor13 = Resistor(positiveTerminal: nodeG, negativeTerminal: nodeD, resistance: 8, name: "R13")
        var circuit = Circuit()
        circuit += [resistor1, resistor2, resistor3, resistor4, resistor5, resistor6, resistor7, resistor8, resistor9, resistor10, resistor11, resistor12, resistor13]
        assertIsValidCircuit(circuit)
        return (circuit, [nodeA, nodeB, nodeC, nodeD, nodeE, nodeF, nodeG, nodeH])
    }
    
    static func createCircuitF() -> (circuit: Circuit, visualElements: [UICircuitElement], points: [CGPoint]) {
        let nodeA = Node(name: "A")
        let nodeB = Node(name: "B")
        let nodeC = Node(name: "C")
        let nodeD = Node(name: "D")
        let nodeE = Node(name: "E")
        
        let resistor1 = Resistor(positiveTerminal: nodeA, negativeTerminal: nodeB, resistance: 25, name: "R1")
        let resistor2 = Resistor(positiveTerminal: nodeB, negativeTerminal: nodeC, resistance: 50, name: "R2")
        let resistor3 = Resistor(positiveTerminal: nodeD, negativeTerminal: nodeB, resistance: 20, name: "R3")
        let resistor4 = Resistor(positiveTerminal: nodeB, negativeTerminal: nodeE, resistance: 15, name: "R4")
        
        let circuit = Circuit(devices: resistor1, resistor2, resistor3, resistor4)
        
        assertIsValidCircuit(circuit)
        
        let pointA = CGPointMake(5, 5)
        let pointB = CGPointMake(10, 10)
        let pointC = CGPointMake(15, 15)
        let pointD = CGPointMake(20, 20)
        let pointE = CGPointMake(25, 25)
        let pointF = CGPointMake(30, 30)
        
        let points = [pointA, pointB, pointC, pointD, pointE, pointF]
        
        // Needs to be created first to reference as wire delegate
        var circuitWrapper = CircuitWrapper(circuit: circuit, visualCircuit: [])
        
        let visualResistor1 = UIResistor(positiveTerminalPoint: pointA, negativeTerminalPoint: pointB, resistor: resistor1)
        let visualResistor2 = UIResistor(positiveTerminalPoint: pointB, negativeTerminalPoint: pointC, resistor: resistor2)
        let visualResistor3 = UIResistor(positiveTerminalPoint: pointD, negativeTerminalPoint: pointF, resistor: resistor3)
        let visualResistor4 = UIResistor(positiveTerminalPoint: pointF, negativeTerminalPoint: pointE, resistor: resistor4)
        
        let wire1 = StraightWire(startPoint: pointB, endPoint: pointF, wireDelegate: circuitWrapper)
        
        circuitWrapper.addVisualElement(visualResistor1, visualResistor2, visualResistor3, visualResistor4, wire1)
        
        return (circuit, circuitWrapper.visualCircuit, points)
        
        
    }
    
    static func createCircuitG() -> Circuit {
        let nodeA = Node(name: "A")
        let nodeB = Node(name: "B")
        let nodeC = Node(name: "C")
        let nodeD = Node(name: "D")
        
        let resistor1 = Resistor(positiveTerminal: nodeA, negativeTerminal: nodeD, resistance: 10, name: "R1")
        let resistor2 = Resistor(positiveTerminal: nodeD, negativeTerminal: nodeC, resistance: 5, name: "R2")
        let resistor3 = Resistor(positiveTerminal: nodeC, negativeTerminal: nodeB, resistance: 30, name: "R3")
        
        let voltageSourceX = DCVoltageSource(voltage: 5, positiveTerminal: nodeA, negativeTerminal: nodeC)
        let voltageSourceY = DCVoltageSource(voltage: 10, positiveTerminal: nodeA, negativeTerminal: nodeB)
        
        let circuit = Circuit(devices: resistor1, resistor2, resistor3, voltageSourceX, voltageSourceY)
        return circuit
    }
    
    static func createCircuitH() -> (Circuit, [Node], Resistor, VoltageSource) {
        let nodeA = Node(name: "A")
        let nodeB = Node(name: "B")
        let resistor = Resistor(positiveTerminal: nodeA, negativeTerminal: nodeB, name: "R1")
        let voltageSource = DCVoltageSource(voltage: 5, positiveTerminal: nodeA, negativeTerminal: nodeB)
        
        let circuit = Circuit(devices: resistor, voltageSource)
        return (circuit, [nodeA, nodeB], resistor, voltageSource)
    }
    
    static func assertIsValidCircuit(circuit: Circuit) {
        var usedNodes: Set<Node> = []
        for device in circuit.devices {
            XCTAssert(circuit.nodes.contains(device.positiveNode))
            XCTAssert(circuit.nodes.contains(device.negativeNode))
            usedNodes += [device.positiveNode, device.negativeNode]
        }
        for node in circuit.nodes {
            XCTAssert(usedNodes.contains(node))
            usedNodes.remove(node)
        }
        XCTAssert(usedNodes.count == 0)
    }
    
}