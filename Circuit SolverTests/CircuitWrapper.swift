//
//  CircuitWrapper.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/1/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import UIKit

// Designed to be used exclusively for testing
struct CircuitWrapper: WireDelegate {
    let circuit: Circuit
    var visualCircuit: [UICircuitElement]
    
    var circuitGraph: CircuitGraph {
        get {
            return CircuitGraph(list: self.visualCircuit)
        }
    }
    
    init(circuit: Circuit, visualCircuit: [UICircuitElement]) {
        self.circuit = circuit
        self.visualCircuit = visualCircuit
    }
    
    init(circuit: Circuit) {
        self.init(circuit: circuit, visualCircuit: [])
    }
    
    mutating func addVisualElement(elements: UICircuitElement...) {
        for element in elements {
            self.visualCircuit.append(element)
        }
    }
    
    func allWires(node: Node) -> [Wire] {
        var list = [Wire]()
        for element in self.visualCircuit {
            if let wire = element as? Wire {
                if let wireNode = wire.node {
                    if wireNode == node {
                        list.append(wire)
                    }
                }
            }
        }
        return list
    }
}
