//
//  CircuitTests.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/14/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit
import XCTest


class CircuitTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
    
    
    func assertOnlyResistors(circuit: Circuit) {
        for device in circuit.devices {
            XCTAssert(device.isKindOfClass(Resistor))
        }
    }
        
    func testCloneCircuitWithoutSources() {
        let circuit = CircuitLibrary.createCircuitB()
        println("Circuit B: \(circuit.description)")
        let circuitNoSource = CircuitMutator(circuit: circuit).circuitNoSources
        assertOnlyResistors(circuitNoSource)
        XCTAssert(circuitNoSource.devices.count == 1)
        println("Circuit No Source: \(circuitNoSource.description)")
        XCTAssert(circuitNoSource.nodes.count == 2, "Circuit: \(circuitNoSource.nodes.count)")
        CircuitLibrary.assertIsValidCircuit(circuitNoSource)
        XCTAssert(CircuitSolver(circuit: circuitNoSource).isValid)
        
    }
    
    func testClone() {
        let originalCircuit = CircuitLibrary.createCircuitB()
        let clonedCircuit = originalCircuit.clone()
        XCTAssert(originalCircuit.devices == clonedCircuit.devices)
        XCTAssert(originalCircuit.nodes == clonedCircuit.nodes, "Original: \(originalCircuit.nodes.description) == Cloned: \(clonedCircuit.nodes.description)")
        CircuitLibrary.assertIsValidCircuit(clonedCircuit)
    }
    
    func testSeriesResistorsCollapseSimple() {
        
        // Construct Circuit Inline to provide access to the individual nodes
        var (circuit, nodes) = CircuitLibrary.createCircuitC()
        
        XCTAssert(CircuitSolver(circuit: circuit).isValid)
        var resistance = EquivalentResistanceSolver(circuit: circuit, nodeA: nodes[0], nodeB: nodes[2]).solve() //circuit.calculateEquivalentResistance(nodes[0], inputB: nodes[2])
        XCTAssert(resistance == 10)
        (circuit, nodes) = CircuitLibrary.createCircuitC()
        resistance = EquivalentResistanceSolver(circuit: circuit, nodeA: nodes[0], nodeB: nodes[1]).solve() //circuit.calculateEquivalentResistance(nodes[0], inputB: nodes[1])
        XCTAssert(resistance == 4)
        (circuit, nodes) = CircuitLibrary.createCircuitC()
        resistance = EquivalentResistanceSolver(circuit: circuit, nodeA: nodes[1], nodeB: nodes[2]).solve() //circuit.calculateEquivalentResistance(nodes[1], inputB: nodes[2])
        XCTAssert(resistance == 6)
    }
    
    func testSeriesAndParallelResistorsCollapseSimple() {
        var (circuit, nodes) = CircuitLibrary.createCircuitD()
        XCTAssert(CircuitSolver(circuit: circuit).isValid)
        var resistance = EquivalentResistanceSolver(circuit: circuit, nodeA: nodes[0], nodeB: nodes[1]).solve()
        XCTAssert(resistance ~== 2.4, "Resistance Equality Failed: Expected: 2.4 Actual: \(resistance)")
        println("Test 1 Pass")
        //(circuit, nodes) = CircuitLibrary.createCircuitD()
        resistance = EquivalentResistanceSolver(circuit: circuit, nodeA: nodes[0], nodeB: nodes[2]).solve()
        XCTAssert(resistance ~== 12.4, "Resistance Equality Failed: Expected: 12.4 Actual: \(resistance)")
        println("Test 2 Pass")
        //(circuit, nodes) = CircuitLibrary.createCircuitD()
        resistance = EquivalentResistanceSolver(circuit: circuit, nodeA: nodes[0], nodeB: nodes[3]).solve()
        XCTAssert(resistance ~== 17.2, "Resistance Equality Failed: Expected: 17.2 Actual: \(resistance)")
        println("Test 3 Pass")
        //(circuit, nodes) = CircuitLibrary.createCircuitD()
        resistance = EquivalentResistanceSolver(circuit: circuit, nodeA: nodes[1], nodeB: nodes[2]).solve()
        XCTAssert(resistance ~== 10, "Resistance Equality Failed: Expected: 10 Actual: \(resistance)")
        println("Test 4 Pass")
        //(circuit, nodes) = CircuitLibrary.createCircuitD()
        resistance = EquivalentResistanceSolver(circuit: circuit, nodeA: nodes[1], nodeB: nodes[3]).solve()
        XCTAssert(resistance ~== 14.8, "Resistance Equality Failed: Expected: 14.8 Actual: \(resistance)")
        println("Test 5 Pass")
        //(circuit, nodes) = CircuitLibrary.createCircuitD()
        resistance = EquivalentResistanceSolver(circuit: circuit, nodeA: nodes[2], nodeB: nodes[3]).solve()
        XCTAssert(resistance ~== 4.8, "Resistance Equality Failed: Expected: 4.8 Actual: \(resistance)")
        println("Test 6 Pass")
        
    }
    
    func testEquivalentResistanceComplex() {
        
        println("Complex Test Start\n\n\n")
        //Calculated by swift library with expression 5 || ( 10 + ((20 || 30) || (20 + 31 + ( 8 || (4 + (10 || 8 || 6))))))
        let expectedEquivalentResistance = 3.99347424163688
        let (circuit, nodes) = CircuitLibrary.createCircuitE()
        let resistance = EquivalentResistanceSolver(circuit: circuit, nodeA: nodes[0], nodeB: nodes[1]).solve()
        
        XCTAssert(expectedEquivalentResistance ~== resistance, "Calculated Value: \(resistance) Expected Value: \(expectedEquivalentResistance)")
    }
    
    func testMinCircuit() {
        let minCircuit = CircuitLibrary.createCircuitH().0 // Ripping the circuit out of the tuple for this test
        XCTAssert(minCircuit.isMinCircuit, "This should be a min circuit")
        var minElements = minCircuit.minElements
        XCTAssert(minElements.source is DCVoltageSource, "Found a DC Voltage Source")
        
    }

}
