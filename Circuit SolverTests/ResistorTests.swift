//
//  ResistorTests.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/14/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit
import XCTest

class ResistorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
    func testInit() {
        var positiveNode = Node(voltage: 5)
        var negativeNode = Node(voltage: 0)
        let resistor = Resistor(positiveTerminal: positiveNode, negativeTerminal: negativeNode, resistance: 50)
        XCTAssert(resistor.currentThrough == 0.1)
        XCTAssert(resistor.voltageAcross == 5)
        XCTAssert(resistor.resistance == 50)
    }
    
    func testParallelOperator() {
        let nodeA = Node(name: "A")
        let nodeB = Node(name: "B")
        let resistor1 = Resistor(positiveTerminal: nodeA, negativeTerminal: nodeB, resistance: 6)
        let resistor2 = Resistor(positiveTerminal: nodeB, negativeTerminal: nodeA, resistance: 12)
        let resistor1ParallelResistor2 = resistor1 || resistor2
        XCTAssert(resistor1ParallelResistor2.resistance == 4.0)
        XCTAssert(ResistorTests.orderInsignficantTerminalEquality(resistor1ParallelResistor2, node1: nodeA, node2: nodeB))
        
    }
    
    class func orderInsignficantTerminalEquality(device: Device, node1: Node, node2: Node) -> Bool {
        return device.positiveNode == node1 && device.negativeNode == node2 || device.positiveNode == node2 && device.negativeNode == node1
    }
    
    func testSeriesOperator() {
        let nodeA = Node(name: "A")
        let nodeB = Node(name: "B")
        let nodeC = Node(name: "C")
        let resistor1 = Resistor(positiveTerminal: nodeA, negativeTerminal: nodeB, resistance: 20)
        let resistor2 = Resistor(positiveTerminal: nodeB, negativeTerminal: nodeC, resistance: 30)
        let resistor1SeriesResistor2 = resistor1 + resistor2
        XCTAssert(resistor1SeriesResistor2.resistance == 50.0)
        XCTAssert(ResistorTests.orderInsignficantTerminalEquality(resistor1SeriesResistor2, node1: nodeA, node2: nodeC))
        
    }

}
