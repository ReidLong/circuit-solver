//
//  CircuitGraphTests.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/1/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import UIKit
import XCTest

class CircuitGraphTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testPath() {
        let (circuit, visualCircuit, points) = CircuitLibrary.createCircuitF()
        let circuitGraph = CircuitGraph(list: visualCircuit)
        
        XCTAssert(circuitGraph.path(fromPoint: points[1], toPoint: points[5]), "Path across wire error")
        XCTAssert(!circuitGraph.path(fromPoint: points[0], toPoint: points[1]), "Path across resistor error")
        
        var brokenList = visualCircuit.copy()
        let removedWire = brokenList.removeLast()
        XCTAssert(removedWire.isKindOfClass(Wire), "This should be the only wire in the circuit")
        let brokenGraph = CircuitGraph(list: brokenList)
        XCTAssert(!brokenGraph.path(fromPoint: points[1], toPoint: points[5]), "Path across removed wire")
        
    }
    
    func testFloodFill() {
        let (circuit, visualCircuit, points) = CircuitLibrary.createCircuitF()
        let circuitGraph = CircuitGraph(list: visualCircuit)
        
        let floodSmall = circuitGraph.floodFill(points[0])
        XCTAssert(floodSmall.count == 1, "Cannot jump over resistors")
        XCTAssert(floodSmall[0] == visualCircuit[0], "These should be resistor 1")
        let floodLarge = circuitGraph.floodFill(points[1])
        
        XCTAssert(floodLarge.count == 5, "All devices selected Count:\(floodLarge.count)")
    }

}
