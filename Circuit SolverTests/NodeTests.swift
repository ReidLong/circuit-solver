//
//  NodeTests.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/14/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit
import XCTest


class NodeTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
    func testClone() {
        let testNode = Node()
        testNode.voltage = 5
        let clonedNode = testNode.clone()
        assertClonedNode(testNode, node2: clonedNode)
    }
    
    func assertClonedNode(node1: Node, node2: Node) {
        XCTAssert(node1.voltage == node2.voltage, "Voltage Cloned")
        XCTAssert(node1 !== node2, "Reference Check")
        XCTAssert(node2.devices.isEmpty, "No Devices Cloned")
    }
    
    func makeListOfNodes(number: Int) -> [Node] {
        var list: [Node] = []
        for i in 1...number {
            var node = Node()
            node.voltage = Double(arc4random()) % 20
            list.append(node)
        }
        assert(list.count == number)
        return list
    }
    
    func testCloneNodes() {
        var nodeList:[Node] = makeListOfNodes(5)
        let clonedNodes = Node.cloneNodes(nodeList)
        XCTAssert(nodeList.count == clonedNodes.count, "Length Preseved")
        for (original, clone) in clonedNodes {
            XCTAssert(nodeList.contains(original), "Nodes Preserved")
            assertClonedNode(original, node2: clone)
        }
        
    }
    
    func testSharedDevices() {
        let nodeA = Node(name: "A")
        let nodeB = Node(name: "B")
        let nodeC = Node(name: "C")
        let device1 = Resistor(positiveTerminal: nodeA, negativeTerminal: nodeB, resistance: 10)
        let device2 = Resistor(positiveTerminal: nodeA, negativeTerminal: nodeC, resistance: 5)
        let device3 = Resistor(positiveTerminal: nodeC, negativeTerminal: nodeB, resistance: 5)
        
        let shared = nodeA.sharedDevices(nodeB)
        XCTAssert(shared.count == 1, "Only 1 Device Shared")
        XCTAssert(shared[0] == device1, "Device 1 is Shared")
        
    }

}
