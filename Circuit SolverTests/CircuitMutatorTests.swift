//
//  CircuitMutatorTest.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/8/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import UIKit
import XCTest

class CircuitMutatorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
    
    func sourceCount(circuit: Circuit) -> Int {
        var count = 0
        for device in circuit.devices {
            if device as? Source != nil {
                count++
            }
        }
        return count
    }
    
    func testCircuitA() {
        let circuitA = CircuitLibrary.createCircuitA()
        let mutator = CircuitMutator(circuit: circuitA)
        let expectNoSources = mutator.circuitNoSources
         XCTAssert(sourceCount(expectNoSources) == 0, "Too many sources")
        let expect2Circuits = mutator.subCircuits
        XCTAssert(expect2Circuits.count == 2, "Circuits: \(expect2Circuits.description)")
        for circuit in expect2Circuits {
            XCTAssert(sourceCount(circuit) == 1, "Circuit: \(circuit.description)")
        }
    }
    
    func testCircuitG() {
        let circuitG = CircuitLibrary.createCircuitG()
        let mutator = CircuitMutator(circuit: circuitG)
        let expectNoSources = mutator.circuitNoSources
        XCTAssert(sourceCount(expectNoSources) == 0, "Circuit No Sources: \(expectNoSources.description)")
        let expect2Circuits = mutator.subCircuits
        XCTAssert(expect2Circuits.count == 2, "Circuits: \(expect2Circuits.description)")
        for circuit in expect2Circuits {
            XCTAssert(sourceCount(circuit) == 1, "Circuit: \(circuit.description)")
        }
    }

}
