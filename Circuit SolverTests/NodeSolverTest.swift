//
//  NodeSolverTest.swift
//  Circuit Solver
//
//  Created by Reid Long on 2/2/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import UIKit
import XCTest

class NodeSolverTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testNodeSolverSimple() {
        let (simpleCircuit, nodes, resistor, source) = CircuitLibrary.createCircuitH()
        nodes[1].voltage = 0; // Setting Node B to ground (this will make the testing easier)
        let solver = NodeSolver(circuit: simpleCircuit)
        XCTAssert(solver.isValid, "Circuit Not Valid")
        let output = solver.solve()
        XCTAssert(output, "Solving Failed")
        XCTAssert(resistor.resistance == 100, "This should be default: \(resistor.resistance)")
        XCTAssert(resistor.voltageAcross == 5, "Voltage Error: \(resistor.voltageAcross)")
        XCTAssert(resistor.currentThrough == 0.05, "Current Error: \(resistor.currentThrough)")
        XCTAssert(source.voltageAcross == 5, "Something really bad happended: \(source.voltageAcross)")
        XCTAssert(source.currentThrough == 0.05, "Current Error: \(source.currentThrough)")
        XCTAssert(nodes[0].name! == "A", "Name Check: \(nodes[0].name!)")
        XCTAssert(nodes[1].name! == "B", "Name Check: \(nodes[1].name!)")
        XCTAssert(nodes[0].voltage! == 5, "Node Error: \(nodes[0].voltage!)")
        XCTAssert(nodes[1].voltage! == 0, "Node Error: \(nodes[1].voltage!)")
        
        
    }

}
