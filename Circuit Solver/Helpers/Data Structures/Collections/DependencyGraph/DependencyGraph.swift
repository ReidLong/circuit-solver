//
//  DependencyGraph.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/11/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

struct DependencyGraph<T: Hashable> {
    private var keyBinder: [T: Int]
    private var lastKey = 0
    private var data: [Int]
    
    init() {
        self.keyBinder = [:]
        self.data = []
    }
    
    mutating func add(element value: T, dependsOn key: T) {
        assert(self.keyBinder[value] == nil, "Already Exists In Dependency Graph")
        
        if let keyRepresentative = self.keyBinder[key] {
            if self.data[keyRepresentative] >= 0 {
                
            }
        }
    }
}