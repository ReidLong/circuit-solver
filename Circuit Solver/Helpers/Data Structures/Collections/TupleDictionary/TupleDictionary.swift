//
//  TupleDictionary.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/5/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

// This is a dictionary structure which will only store one copy of the tuple regardless of orientation
// This struct can be used to remove duplicates in a [(T, T)] list 
struct TupleDictionary<T: Hashable> {
    var internalDictionary: [T: [T: Bool]]
    
    init() {
        self.internalDictionary = [T: [T: Bool]]()
    }
    
    init(list: [(T, T)]) {
        self.init()
        for element in list {
            self.add(element)
        }
    }
    
    func symmetricContains(element1: T, _ element2: T) -> Bool {
        return self.contains((element1, element2)) || self.contains((element2, element1))
    }
    
    func contains(element: (T, T)) -> Bool {
        if let firstLevel = self.internalDictionary[element.0] {
            if firstLevel[element.1] != nil {
                return true
            }
        }
        return false
    }
    
    mutating func add(element: (T, T)) {
        
        let primaryKey = element.0
        let secondaryKey = element.1
        
        if !symmetricContains(primaryKey, secondaryKey) {
            // Don't add element if it already exists
        }
        var contents = self.safeGet(primaryKey)
        contents[secondaryKey] = true
        self.internalDictionary[primaryKey] = contents
    }
    
    func safeGet(key: T) -> [T: Bool] {
        if let safeContents = self.internalDictionary[key] {
            return safeContents
        } else {
            return [T: Bool]() // Return an empty dictionary if the key does not exist
        }
    }
    
    var array: [(T, T)] {
        get {
            return TupleDictionary.generateArray(self.internalDictionary)
        }
    }
    
    static func generateArray(dictionary: [T: [T: Bool]]) -> [(T, T)] {
        var output = [(T, T)]()
        for (primaryKey, subDictionary) in dictionary {
            for secondaryKey in subDictionary.keys {
                output.append((primaryKey, secondaryKey))
            }
        }
        return output
    }
    
}