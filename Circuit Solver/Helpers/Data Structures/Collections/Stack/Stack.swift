//
//  Stack.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/9/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

// Acts as a lightweight interface mapping to array methods
struct Stack<T> {
    var data: [T]
    
    init() {
        self.data = []
    }
    
    init(elements: T...) {
        self.init()
        for element in elements {
            self.push(element)
        }
    }
    
    mutating func push(element: T) {
        self.data.append(element)
    }
    
    mutating func pop() -> T {
        assert(data.count > 0, "Empty Stack")
        let value = self.data.removeLast()
        return value
    }
    
    var isEmpty: Bool {
        get {
            return self.data.isEmpty
        }
    }
    
}