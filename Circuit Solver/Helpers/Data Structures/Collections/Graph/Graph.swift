//
//  Graph.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/29/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import Foundation

struct Graph<T: Hashable> {
    typealias Element = T
    var data: [T:Set<T>] // I am so cool nesting custom data structures
    
    init() {
        self.data = [:]
    }
    
    mutating func addConnection(#fromVertex: T, toVertex: T) {
        var currentList = data[fromVertex]
        if currentList == nil {
            currentList = Set<T>() // also valid as []
        }
        assert(currentList != nil, "This should be explicitly safe")
        if var safeCurrentList = currentList {
            if !safeCurrentList.contains(toVertex) {
                safeCurrentList.add(toVertex)
            }
            data[fromVertex] = safeCurrentList
        }
        
    }
    
    mutating func addBidirectionalConnection(vertexA: T, _ vertexB: T) {
        self.addConnection(fromVertex: vertexA, toVertex: vertexB)
        self.addConnection(fromVertex: vertexB, toVertex: vertexA)
    }
    
    func hasConnection(#fromVertex: T, toVertex: T) -> Bool {
        if let currentList = data[fromVertex] {
            if currentList.contains(toVertex) {
                return true
            }
        }
        return false
        
    }
}