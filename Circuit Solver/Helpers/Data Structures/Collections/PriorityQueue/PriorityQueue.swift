//
//  PriorityQueue.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/1/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation


class PriorityQueue<T: Hashable> {
    private var data: [T?]
    private var emptyIndex: Int // Used to make calculations easy
    private var higherPriority: (T, T) -> Bool
    
    init(higherPriority: (T, T) -> Bool) {
        self.data = [nil] // junk data at index 0
        self.higherPriority = higherPriority
        self.emptyIndex = 1 // next usable index is 1
    }
    
    var count: Int {
        return self.emptyIndex - 1
    }
    var isEmpty: Bool {
        return self.emptyIndex == 1
    }
    
    func contains(element: T) -> Bool {
        for item in self.data {
            if item == element {
                return true
            }
        }
        return false
    }
    
    private func parentCheck(parentIndex: Int, childIndex: Int) -> Bool {
        if let parent = self.data[parentIndex] {
            if let child = self.data[childIndex] {
                return !self.higherPriority(child, parent)
            }
        }
        assert(false, "If this line executes, the data structure is malformed")
        return false
    }
    
    private func swapUp(index: Int) {
        let parentIndex = index / 2
        let temp = self.data[index]
        self.data[index] = self.data[parentIndex]
        self.data[parentIndex] = temp
        
    }

    // Please don't add elements that already exist in the data structure
    func add(element: T) {
        var index = self.emptyIndex
        if index < self.data.count {
            self.data[index] = element
        } else {
            self.data.append(element)
        }
        self.emptyIndex++
    
        while (index > 1) {
            if parentCheck(index / 2, childIndex: index) {
                return //Done
            }
            swapUp(index)
            index /= 2
        }
    }
    
    // This may be optimizable, but it is probably not a significant boost
    func add(elements: [T]) {
        for element in elements {
            self.add(element)
        }
    }
    
    func siftDown() {
        var index = 1
        while ( 2 * index < self.emptyIndex) {
            let left = 2 * index
            let right = left + 1
            
            if parentCheck(index, childIndex: left) && (right >= self.emptyIndex || parentCheck(index, childIndex: right)) {
                return // Done
            }
            if right >= self.emptyIndex || parentCheck(left, childIndex: right) {
                swapUp(left)
                index = left
            } else {
                assert(right < self.emptyIndex && parentCheck(right, childIndex: left), "Logical Error")
                swapUp(right)
                index = right
            }
        }
    }
    
    func remove() -> T? {
        if self.isEmpty {
            println("Dequeuing from empty queue")
            return nil
        }
        let min = self.data[1]
        self.emptyIndex--
        
        if !self.isEmpty {
            self.data[1] = self.data[self.emptyIndex]
            // Shrink array if the emptyIndex << array.count
            siftDown()
        }
        return min
    }
}