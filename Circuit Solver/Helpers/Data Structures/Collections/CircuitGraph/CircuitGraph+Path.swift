//
//  CircuitGraph+Path.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/30/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

extension CircuitGraph {
    // Required to be mutating becuase the dictionary must be modified during execution
    func path(#fromPoint: CGPoint, toPoint: CGPoint) -> Bool {
        
        var mutableDictionary = DictionaryReference(dictionary: self.dictionary)
        
        return CircuitGraph.pathHelper(fromPoint: fromPoint, toPoint: toPoint, mutableDictionary: mutableDictionary)
    }
    
    private static func pathHelper(#fromPoint: CGPoint, toPoint: CGPoint, var mutableDictionary: DictionaryReference<CGPoint, Set<UICircuitElement>>) -> Bool {
        
        if fromPoint == toPoint {
            return true
        }
        if let elements = mutableDictionary.dictionary[fromPoint] {
            mutableDictionary.dictionary[fromPoint] = nil
            for element in elements {
                if element.isKindOfClass(Wire) {
                    let otherPoint = fromPoint.uniqueSinglton(element.startPoint, element.endPoint)
                    let foundPath = pathHelper(fromPoint: otherPoint, toPoint: toPoint, mutableDictionary: mutableDictionary)
                    if foundPath {
                        // Rebuild data structure
                        mutableDictionary.dictionary[fromPoint] = elements
                        return true
                    }
                    
                }
            }
            mutableDictionary.dictionary[fromPoint] = elements
        }
        
        return false
    }
    
}
