//
//  CircuitGraph+FloodFill.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/31/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

extension CircuitGraph {
    func floodFill(startingPoint: CGPoint) -> [UICircuitElement] {
        var mutableDictionary = DictionaryReference(dictionary: self.dictionary)
        
        return CircuitGraph.floodFillHelper(startingPoint, mutableDictionary: mutableDictionary).array
    }
    
    private static func floodFillHelper(startingPoint: CGPoint, var mutableDictionary: DictionaryReference<CGPoint, Set<UICircuitElement>>) -> Set<UICircuitElement> {
        var output = Set<UICircuitElement>()
        if let elements = mutableDictionary.dictionary[startingPoint] {
            mutableDictionary.dictionary[startingPoint] = nil
            for element in elements {
                if element.isKindOfClass(Wire) {
                    let otherPoint = startingPoint.uniqueSinglton(element.startPoint, element.endPoint)
                    let foundItems = floodFillHelper(otherPoint, mutableDictionary: mutableDictionary)
                    output += foundItems
                }
                output.add(element)
            }
            mutableDictionary.dictionary[startingPoint] = elements
        }
        
        return output
        
    }
    
}
