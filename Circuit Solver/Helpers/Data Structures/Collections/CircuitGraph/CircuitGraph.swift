//
//  CircuitGraphStruct.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/30/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

struct CircuitGraph {
    var dictionary: [CGPoint: Set<UICircuitElement>]
    
    init() {
        dictionary = [:]
    }
    
    init(list: [UICircuitElement]) {
        self.init()
        for element in list {
            self.add(element)
        }
    }
    
    mutating func add(element: UICircuitElement) {
        self.add(key: element.startPoint, element: element)
        self.add(key: element.endPoint, element: element)
    }
    
    private mutating func add(#key: CGPoint, element: UICircuitElement) {
        assert(key == element.startPoint || key == element.endPoint, "The point should directly associate with the element")
        var currentData = self.safeAccess(key)
        currentData.add(element)
        self.dictionary[key] = currentData
    }
    
    private func safeAccess(key: CGPoint) -> Set<UICircuitElement> {
        var rawData = self.dictionary[key]
        if rawData == nil {
            rawData = Set<UICircuitElement>()
        }
        assert(rawData != nil, "Explicitly true")
        return rawData!
    }
}
