//
//  Queue.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/5/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

struct Queue<T> {
    
    var head: LinkedListNode<T>?
    var tail: LinkedListNode<T>?
    
    var size: UInt = 0
    
    init() {
        self.head = nil
        self.tail = nil
    }
    
    var count: UInt {
        return self.size
    }
    
    var isEmpty: Bool {
        return self.head == nil && self.tail == nil
    }
    
    mutating func enqueue(element: T) {
        let node = LinkedListNode(value: element)
        if self.head == nil {
            assert(self.tail == nil, "Both should be nil for empty queue")
            self.head = node
        }
        self.tail?.next = node
        self.tail = node
    }
    
    mutating func dequeue() -> T {
        assert(!self.isEmpty, "Better have items in queue if you are dequeueing")
        let node = self.head
        self.head = self.head!.next
        if self.head == nil {
            self.tail = nil // Keep the data structure pure
        }
        return node!.value
    }

}