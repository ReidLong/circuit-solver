//
//  LinkedListNode.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/5/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

// This should probably be a struct, but Swift is complaining about recursive struct definitions
class LinkedListNode<T> {
    var value: T
    var next: LinkedListNode<T>? = nil
    
    init(value: T) {
        self.value = value
    }
    
    convenience init(value: T, next: LinkedListNode<T>) {
        self.init(value: value)
        self.next = next
    }
    
}