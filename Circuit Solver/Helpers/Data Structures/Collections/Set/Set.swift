//
//  Set.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/14/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import Foundation

let NOTFOUND = -1

struct Set<T: Hashable> {
   
    typealias Element = T
    private var data: [T]
    
    // MARK: Initialization
    init() {
        self.data = [T]()
    }
    
    // MARK: Properties
    var count: Int {
        return data.count
    }
    var isEmpty: Bool {
        return data.isEmpty
    }
    var array: [T] {
        return self.data
    }
    // MARK: Standard Utilities
    func contains(element: T) -> Bool {
        return self.getIndex(element) >= 0
    }
    mutating func add(newElement: T) -> T? {
        let itemIndex = self.getIndex(newElement)
        if itemIndex == NOTFOUND {
            self.data.append(newElement)
            return nil
        } else {
            let oldData = self.data[itemIndex]
            self.data[itemIndex] = newElement
            return oldData
        }
    }
    mutating func remove(element: T) -> T? {
        let itemIndex = self.getIndex(element)
        if itemIndex == NOTFOUND {
            return nil
        } else {
            let oldData = self.data.removeAtIndex(itemIndex)
            assert(oldData == element, "Equal Behavior Going Crazy")
            return oldData
            
        }
    }
    
    func get(element: T) -> T? {
        let itemIndex = self.getIndex(element)
        if itemIndex >= 0 {
            return self.data[itemIndex]
        } else {
            return nil
        }
    }
    
    private func getIndex(element: T) -> Int {
        for index in 0..<self.data.count {
            let item = self.data[index]
            if item == element {
                return index
            }
        }
        return NOTFOUND
    }
    
    
    
}

extension Set : SequenceType {
    
    //Required for interating with for-in loops
    //typealias Generator = MapSequenceGenerator<DictionaryGenerator<T, T>, T>
    typealias Generator = IndexingGenerator<Array<T>>
    func generate() -> Generator {
        return self.data.generate()
    }
}

extension Set {
    
    /*
        Example Usage
        let set1 = Set([1, 2, 3, 4, 5])
        let set2 = Set(1...10)
        let set3 = "abcdef"
    */
    init<S: SequenceType where S.Generator.Element == T>(_ sequence: S) {
        self.init()
        map(sequence) {
            self.add($0)
        }
    }
}

extension Set : ArrayLiteralConvertible {
    /*
        Examples
        let set: Set = [1, 2, 3, 4]
    */
    init(arrayLiteral elements: Element...) {
        self.init(elements)
    }
}

extension Set {
    func isEqualToSet(set: Set<T>) -> Bool {
        // For some reason, self.data == set.data is not using the overridden equality opperator
        println("Calling broken equality opperator: \(self.data.description) == \(set.data.description)")
        return self.data.description == set.data.description
        //return self.data == set.data
    }
    mutating func unionSet(set: Set<T>) {
        for element in set {
            self.add(element)
        }
    }
    mutating func appendSet(set: Set<T>) {
        self.unionSet(set)
    }
}

//Operators must be implemented at the global scope
func +=<T>(inout lhs: Set<T>, rhs: T) {
    lhs.add(rhs)
}
    
func +=<T>(inout lhs: Set<T>, rhs: Set<T>) {
    lhs.unionSet(rhs)
}





func +<T>(inout lhs: Set<T>, rhs: Set<T>) -> Set<T> {
    lhs.unionSet(rhs)
    return lhs
}
func ==<T>(lhs: Set<T>, rhs: Set<T>) -> Bool {
    return lhs.isEqualToSet(rhs)
}

func -=<T>(inout lhs: Set<T>, rhs: T) {
    lhs.remove(rhs)
}

extension Set : Equatable {
    
}



extension Set : Printable, DebugPrintable {
    var description: String {
        return "Set(\(self.array))"
    }
    
    var debugDescription: String {
        return self.description
    }
    
}