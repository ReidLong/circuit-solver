//
//  Rectangle.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/24/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit


struct Rectangle {
    var points: [CGPoint]
    init(points: [CGPoint]) {
        assert(points.count == 4, "Points Do not define rectangle")
        self.points = points
    }
    
    func sides() -> [(CGPoint, CGPoint)] {
        var list: [(CGPoint, CGPoint)] = []
        for startIndex in 0..<points.count {
            let endIndex = (startIndex + 1) % points.count //Modulus to support wrap around
            let pointTuple = (self.points[startIndex], self.points[endIndex])
            list.append(pointTuple)
        }
        
        return list
    }
    
    var width: CGFloat {
        get {
            return points[0].distance(fromPoint: points[1])
        }
    }

    var height: CGFloat {
        get {
            return points[1].distance(fromPoint: points[2])
        }
    }
    
    var area: CGFloat {
        get {
            return self.width * self.height
        }
    }
}
