//
//  Vector.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/19/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

let π = M_PI

struct Vector: Printable, DebugPrintable {
    //Angle must be in radians
    var angle: CGFloat
    var magnitude: CGFloat
    
    var deltaX: CGFloat {
        get {
            return magnitude * cos(angle)
        }
    }
    var deltaY: CGFloat {
        get {
            return magnitude * sin(angle)
        }
    }
    init(magnitude: CGFloat, angle: CGFloat) {
        if angle > CGFloat(2 * π) {
            println("Are you using radians for angles?")
        }
        self.angle = angle
        self.magnitude = magnitude
    }
    
    var description: String {
        return "Vector: (\(magnitude), \(angle)) - (\(self.deltaX), \(self.deltaY))"
    }
    var debugDescription: String {
        return self.description
    }
    
    static func unitVector(angle: CGFloat) -> Vector {
        return Vector(magnitude: 1, angle: angle)
    }
}

