//
//  Triangle.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/24/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

struct Triangle {

    var points: [CGPoint]
    
    init(points: [CGPoint]) {
        assert(points.count == 3, "Point Count Missmatch")
        self.points = points
    }

    var area: CGFloat {
        get {
            return self.calculateArea()
        }
    }

    func calculateArea() -> CGFloat {
        let A = points[0]
        let B = points[1]
        let C = points[2]
        
        let area = abs((A.x * (B.y - C.y) + B.x * (C.y - A.y) + C.x * (A.y - B.y)) * 0.5)

        return area
    }
}