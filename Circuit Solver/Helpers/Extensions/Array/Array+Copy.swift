//
//  Array+Copy.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/29/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import Foundation

extension Array {
    func copy() -> [T] {
        var output = [T]()
        for element in self {
            output.append(element)
        }
        return output
    }
}