//
//  Array+Contains.swift
//  Circuit Solver
//
//  Created by Reid Long on 10/22/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import Foundation

extension Array {
    func contains(object: T) -> Bool {
        if let safeObject = object as? NSObject {
            for data in self {
                if let safeData = data as? NSObject {
                    if safeObject == safeData {
                        return true
                    }
                }
            }
        }
        return false
    }
}