//
//  Pair.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/11/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

struct Pair<T: Hashable>: Hashable {
    let data: (T, T)
    init(tuple: (T, T)) {
        self.data = tuple
    }
    
    var hashValue: Int {
        get {
            return data.0.hashValue + data.1.hashValue
        }
    }
}

func ==<T: Hashable>(lhs: Pair<T>, rhs: Pair<T>) -> Bool {
    return true
}