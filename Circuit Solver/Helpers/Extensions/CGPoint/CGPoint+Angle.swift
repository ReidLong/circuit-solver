//
//  CGPoint+Angle.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/24/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

extension CGPoint {
    static func angle(point1: CGPoint, _ point2: CGPoint) -> CGFloat {
        let denominator = point1.x - point2.x
        let numerator = point1.y - point2.y
        if denominator == 0 {
            return copysign(CGFloat(π / 2), numerator)
        } else {
            return atan(numerator / denominator)
        }
    }
}