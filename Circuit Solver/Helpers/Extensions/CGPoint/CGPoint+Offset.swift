//
//  CGPoint+Offset.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/24/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

extension CGPoint {
    func offset(vector: Vector) -> [CGPoint] {
        // phi = completement of theta
        // X coordinates are offset by the sin of theta (cos of phi)
        // Y coordinates are offset by the cos of theta (sin of phi)
        
        let leftOffset = CGPointMake(self.x + vector.deltaY, self.y - vector.deltaX)
        let rightOffset = CGPointMake(self.x - vector.deltaY, self.y + vector.deltaX)
        
        return [leftOffset, rightOffset]
    }
}