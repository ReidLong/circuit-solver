//
//  CGPoint+Unique.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/30/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

// Operator Documentation:
// The unique opperation selectively returns the points which are NOT the instance the function is called on. 
// This behavior creates a solution for an exclusive or operation on a CGPoint
// let pointA = CGPointMake(...)
// let pointB = CGPointMake(...)
// let pointC = pointA
// let uniquePoint = pointA ^ pointB ^ pointC 
// uniquePoint == pointB
extension CGPoint {
    
    func unique(otherPoints: CGPoint...) -> [CGPoint] {
        return self.uniqueArray(otherPoints)
    }
    
    private func uniqueArray(otherPoints: [CGPoint]) -> [CGPoint] {
        var list = [CGPoint]()
        for point in otherPoints {
            if point != self {
                list.append(point)
            }
        }
        return list
    }
    
    func uniqueSinglton(otherPoints: CGPoint...) -> CGPoint {
        let uniqueList = self.uniqueArray(otherPoints)
        assert(uniqueList.count == 1, "Violates the singleton requirement")
        return uniqueList[0]
    }
}
