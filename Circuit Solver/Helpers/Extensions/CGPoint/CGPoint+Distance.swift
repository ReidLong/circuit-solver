//
//  CGPoint+Distance.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/29/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

extension CGPoint {
    func distance(fromPoint point: CGPoint) -> CGFloat {
        return hypot(self.x - point.x, self.y - point.y)
    }
}
