//
//  CGPoint+Center.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/21/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

extension CGPoint {

    static func center(pointA: CGPoint, _ pointB: CGPoint) -> CGPoint {
        return CGPointMake((pointA.x + pointB.x) / 2, (pointA.y + pointB.y)/2)
    }
}