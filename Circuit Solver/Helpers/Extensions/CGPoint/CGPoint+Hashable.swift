//
//  CGPoint+Hashable.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/30/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

extension CGPoint: Hashable {
    public var hashValue: Int {
        get {
            return Int(self.x + self.y)
        }
    }
}
