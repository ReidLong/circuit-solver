//
//  CGPoint+Intersection.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/30/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

extension CGPoint {
    
    static func intersection(listA: [CGPoint], _ listB: [CGPoint]) -> Bool {
        for point1 in listA {
            for point2 in listB {
                if point1 == point2 {
                    return true
                }
            }
        }
        return false
    }

}
