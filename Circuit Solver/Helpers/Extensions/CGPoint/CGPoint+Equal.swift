//
//  CGPoint+Equal.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/19/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

let RADIUS: CGFloat = 30 //points

func ~==(lhs: CGPoint, rhs: CGPoint) -> Bool {
    let distance = lhs.distance(fromPoint: rhs)
    return distance <= RADIUS
}


