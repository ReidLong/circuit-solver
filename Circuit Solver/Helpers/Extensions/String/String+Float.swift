//
//  String+Float.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/22/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import Foundation

extension String {
    func toFloat() -> Float? {
        
        let nsString = self as NSString
        if NSScanner(string: nsString).scanFloat(nil) {
            return nsString.floatValue
        } else {
            return nil
        }
    }
}