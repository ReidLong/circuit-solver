//
//  String+Regex.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/1/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

extension String {
    
    // Convenience function for routing regular expressions to the native callers
    func matches(#regex: String) -> Bool {
        if let matchRange = self.rangeOfString(regex, options: NSStringCompareOptions.RegularExpressionSearch, range: nil, locale: nil) {
            return true
        }
        return false
    }
}