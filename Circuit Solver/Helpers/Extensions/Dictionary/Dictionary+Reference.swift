//
//  Dictionary+Reference.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/17/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import Foundation

class DictionaryReference<K: Hashable, V> {
    var dictionary: Dictionary<K, V>
    init(dictionary: Dictionary<K, V>) {
        self.dictionary = dictionary
    }
}