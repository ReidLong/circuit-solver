//
//  NumbericType.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/24/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import Foundation

protocol NumericType {
    func +(lhs: Self, rhs: Self) -> Self
    func -(lhs: Self, rhs: Self) -> Self
    func *(lhs: Self, rhs: Self) -> Self
    func /(lhs: Self, rhs: Self) -> Self
    func %(lhs: Self, rhs: Self) -> Self
}

extension Double : NumericType {}
extension Float : NumericType {}
extension Int : NumericType {}
extension Int8 : NumericType {}
extension Int16 : NumericType {}
extension Int32 : NumericType {}
extension Int64 : NumericType {}
extension UInt : NumericType {}
extension UInt8 : NumericType {}
extension UInt16 : NumericType {}
extension UInt32 : NumericType {}
extension UInt64 : NumericType {}