//
//  Float+Format.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/26/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import Foundation

let PERCISION_UPPER_BOUND: Int = 5 //Anything higher than 5 decimal places is ridiculous

extension Float {
    func format(percision: Int = 0) -> String {
        assert(0 <= percision && percision < PERCISION_UPPER_BOUND, "Past Percision Parameter is Invalid: \(percision)")
        return String(format: "%.\(percision)f", arguments: [self])
    }
}