//
//  Double+Equal.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/16/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import Foundation

//Approximately Equal
infix operator ~== { }

let DOUBLE_APPROX_EQUAL_EPSILON = 0.001

func ~==(lhs: Double, rhs: Double) -> Bool {
    return abs(lhs - rhs) < DOUBLE_APPROX_EQUAL_EPSILON // lhs == rhs
}
