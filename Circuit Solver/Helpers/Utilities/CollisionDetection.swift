//
//  CollisionDetection.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/24/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

let PROXIMITY_CONSTANT: CGFloat = 5 //points squared

struct CollisionDetection {
    
    private init() {
        // private init for preventing the instantiation of the CollisionDetection struct
        assert(false)
    }
    
    static func hasCollision(shape: [CGPoint], testPoint pointC: CGPoint) -> Bool {
        assert(shape.count == 4, "Only Supporting Collision Detection with Rectangles")
        
        let rect = Rectangle(points: shape)
        let edgePairs = rect.sides()
        
        var totalArea: CGFloat = 0
        
        for (pointA, pointB) in edgePairs {
            let triangle = Triangle(points: [pointA, pointB, pointC])
            let subArea = triangle.area
            
            totalArea += subArea
        }
        
        return totalArea - rect.area <= PROXIMITY_CONSTANT
        
    }
    
    
}
