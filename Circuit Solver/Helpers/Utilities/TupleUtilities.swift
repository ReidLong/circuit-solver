//
//  TupleUtilities.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/5/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

struct TupleUtilities {
    
    private init() {
        // private init for preventing the instantiation of the TupleUtilities struct
        assert(false)
    }
    
    static func symmetricEquality<T: Equatable>(tupleA: (T, T), _ tupleB: (T, T)) -> Bool {
        // I'm leaving the access indicies instead of replacing them with variables because the indicies make it clear what the patterns are that the function is looking for. 
        return tupleA.0 == tupleB.0 && tupleA.1 == tupleB.1 || tupleA.0 == tupleB.1 && tupleA.1 == tupleB.0
    }
    
    static func symmetricDuplicateRemoval<T: Hashable>(list: [(T, T)]) -> [(T, T)] {
        var dictionary = TupleDictionary(list: list)
        return dictionary.array
    }

}


