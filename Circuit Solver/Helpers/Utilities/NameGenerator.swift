//
//  NameGenerator.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/7/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

let STARTING_CHARACTER: UnicodeScalar = "A"
let MAX_OFFSET = 26 // 26 letters in alphabet

enum NameType {
    case Alphabetic(Int) // Depth
    case Numeric(Int) // Count
}

class NameGenerator {
    let type: NameType
    let prefix: String
    
    init(type: NameType, prefix: String) {
        self.type = type
        self.prefix = prefix
    }
    
    var names: [String] {
        get {
            switch self.type {
            case let NameType.Alphabetic(depth):
                return NameGenerator.generateAlpha(self.prefix, depth: depth)
            case let NameType.Numeric(count):
                return NameGenerator.generateNumeric(self.prefix, count: count)
            }
        }
    }
    
    private class func generateAlpha(prefix: String, depth: Int) -> [String] {
        var list = [prefix]
        for round in 0...depth {
            var tempList = [String]()
            for current in list {
                for offset in 0..<MAX_OFFSET {
                    let character = Character(UnicodeScalar(STARTING_CHARACTER.value + offset))
                    tempList.append("\(current)\(character)")
                }
            }
            list = tempList
        }
        return list
    }
    
    private class func generateNumeric(prefix: String, count: Int) -> [String] {
        assert(false, "Not Implemented Yet")
        return []
    }
}