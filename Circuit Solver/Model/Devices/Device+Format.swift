//
//  Device+Log.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/4/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

extension Device {
    class func format(devices: Set<Device>, seperator: String, prefix: String) -> String {
        var formattedOutput = prefix
        for device in devices {
            // The device should log the node descriptions internally
            formattedOutput += "\(device.description)\(seperator)"
        }
        return formattedOutput
    }
    
}