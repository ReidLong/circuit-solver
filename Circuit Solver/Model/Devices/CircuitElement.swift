//
//  CircuitElement.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/14/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import Foundation

class CircuitElement : NSObject, Printable, DebugPrintable, Equatable {
    var uniqueId: NSUUID
    var name: String? = nil
    
    var safeName: String {
        get {
            if let setName = name {
                return setName
            } else {
                return "(no name)"
            }
        }
    }
    
    override init() {
        self.uniqueId = NSUUID()
        super.init()
    }
    convenience init(name: String) {
        self.init()
        self.name = name
    }
    class func cloneData(hostElement: CircuitElement, newElement: CircuitElement) {
        newElement.uniqueId = hostElement.uniqueId
        newElement.name = hostElement.name
    }
    override var description: String {
        if let nameString = self.name {
            return nameString
            //return "\(nameString) - \(uniqueId.UUIDString)"
        } else {
            return uniqueId.UUIDString
        }
    }
    
    override var debugDescription: String {
        return description
    }
    
    override var hashValue: Int {
        return uniqueId.hashValue
    }
    
    func isEqualCircuitElement(otherCircuitElement: CircuitElement) -> Bool {
        return self.uniqueId == otherCircuitElement.uniqueId
    }
    
    override func isEqual(object: AnyObject?) -> Bool {
        if let otherCircuitElement = object as? CircuitElement {
            return self.isEqualCircuitElement(otherCircuitElement)
        }
        println("Circuit Equality Abort")
        return false
    }
    
    class func sameConcreteType(elements: [CircuitElement]) -> Bool {
        // Edge case to handle empty list
        if elements.isEmpty {
            return true
        }
        let testType = _stdlib_getTypeName(elements.first!) // This function returns a highly convoluted class name.
        
        for element in elements {
            if _stdlib_getTypeName(element) != testType {
                return false
            }
        }
        return true
    }
    
}

func ==<T: CircuitElement>(lhs: T, rhs: T) -> Bool {
    return lhs.isEqualCircuitElement(rhs)
}
