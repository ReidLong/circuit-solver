//
//  Resistor.swift
//  Circuit Solver
//
//  Created by Reid Long on 10/20/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import Foundation

let RESISTANCE_UNIT = "Ω"

// Articifical limits
let MIN_RESISTANCE = 0 //Ω
let MAX_RESISTANCE = 100000 //Ω

class Resistor : Device {
    var resistance: Double = 100//Ω

    
    init(positiveTerminal: Node, negativeTerminal: Node, resistance: Double) {
        self.resistance = resistance
        super.init(positiveTerminal: positiveTerminal, negativeTerminal: negativeTerminal)
    }
    
    init() {
        let dummyNodeA = Node()
        let dummyNodeB = Node()
        super.init(positiveTerminal: dummyNodeA, negativeTerminal: dummyNodeB)
    }
    
    convenience init(positiveTerminal: Node, negativeTerminal: Node, resistance: Double, name: String) {
        self.init(positiveTerminal: positiveTerminal, negativeTerminal: negativeTerminal, resistance: resistance)
        self.name = name
    }
    
    init(positiveTerminal: Node, negativeTerminal: Node, name: String) {
        super.init(positiveTerminal: positiveTerminal, negativeTerminal: negativeTerminal)
        self.name = name
    }
    
    override func clone(positiveNode: Node, negativeNode: Node) -> Device {
        let newResistor = Resistor(positiveTerminal: positiveNode, negativeTerminal: negativeNode, resistance: self.resistance)
        CircuitElement.cloneData(self, newElement: newResistor)
        return newResistor
    }
    
    override var currentThrough: Double {
        get {
            //I = V / R
            return self.voltageAcross / self.resistance
        }
    }
    
    override var description: String {
        get {
            return "Resistor (\(self.resistance)): \(super.description)"
        }
    }
    override func parallel(devicePartner: Device) -> Device {
        if let otherResistor = devicePartner as? Resistor {
            let unionOfNodes = Device.unionNodes([self, otherResistor])
            assert(unionOfNodes.count == 2, "Not Parallel Resistors")
            let newResistanceValue = self.resistance * otherResistor.resistance / (self.resistance + otherResistor.resistance)
            var newResistor = Resistor(positiveTerminal: unionOfNodes[0], negativeTerminal: unionOfNodes[1], resistance: newResistanceValue)
            return newResistor
        }
        assert(false, "Not Other Resistor")
        return self

    }
    
    override func series(devicePartner: Device) -> Device {
        if let otherResistor = devicePartner as? Resistor {
            let nonSharedNodes = Device.uniqueNodes([self, otherResistor])
            assert(nonSharedNodes.count == 2, "Not Series Resistors")
            let newResistanceValue = self.resistance + otherResistor.resistance
            var newResistor = Resistor(positiveTerminal: nonSharedNodes[0], negativeTerminal: nonSharedNodes[1], resistance: newResistanceValue)
            return newResistor
        }
        assert(false, "Not Other Resistor")
        return self
        
    }

    
}
