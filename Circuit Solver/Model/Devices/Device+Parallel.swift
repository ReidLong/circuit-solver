//
//  Device+Parallel.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/5/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

extension Device {
    class func collapseParallel(var devices: [Device]) -> Device {
        assert(devices.count > 0, "Cannot return a device if there are none to start with...")
        while devices.count > 1 {
            let device1 = devices.removeLast()
            let device2 = devices.removeLast()
            devices.append(device1 || device2)
        }
        return devices.first! // Should be safe if the first assertion holds
    }
    
    func parallel(devicePartner: Device) -> Device {
        assert(false, "Fatal Collapse Error Not Impelmented Exception")
        return self
    }
}

func ||<T: Device>(lhs: T, rhs: T) -> T {
    return lhs.parallel(rhs) as T
}