//
//  Device.swift
//  Circuit Solver
//
//  Created by Reid Long on 10/22/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import Foundation


class Device : CircuitElement {
    
    //didSet is not called within initialization context
    var positiveNode: Node {
        didSet {
            oldValue.devices -= self
            positiveNode.devices += self
            
        }
    }
    
    //didSet is not called within initialization context
    var negativeNode: Node {
        didSet {
            oldValue.devices -= self
            negativeNode.devices += self
            
        }
    }
    
    var nodes: (Node, Node) {
        get {
            return (self.positiveNode, self.negativeNode)
        }
    }

    
    // init synchronizes devices with nodes
    init(positiveTerminal: Node, negativeTerminal: Node) {
        positiveNode = positiveTerminal
        negativeNode = negativeTerminal
        super.init()
        //Manual setting required because didSet not called in initialization context
        positiveNode.devices.add(self)
        negativeNode.devices.add(self)
    }
    
    func clone(positiveNode: Node, negativeNode: Node) -> Device {
        let clonedDevice = Device(positiveTerminal: positiveNode, negativeTerminal: negativeNode)
        CircuitElement.cloneData(self, newElement: clonedDevice)
        return clonedDevice
    }
    
    var voltageAcross: Double {
        get {
            if let postiveVoltage = positiveNode.voltage {
                if let negativeVoltage = negativeNode.voltage {
                    return postiveVoltage - negativeVoltage
                }
            }
            NSLog("Attempting to calculate voltage across device that has unsolved nodes")
            return 0
        }
    }
    
    var currentThrough: Double {
        get {
            assert(false, "Attempting to calculate current through device that is not properly configured")
            return 0
        }
    }
    
    class func devicesInSeries(devices: [Device]) -> Bool {
        if devices.count != 2 {
            println("Warning, collapsing more than 2 devices in series is not fully tested")
        }
        
        let classType: AnyClass! = object_getClass(devices[0])
        for device in devices {
            if !device.isKindOfClass(classType) {
                return false
            }
        }
        let unique = uniqueNodes(devices)
        if unique.count != 2 {
            return false
        }
        
        return true
    }
    

    
    class func uniqueNodes(devices: [Device]) -> [Node] {
        var dictionary = Dictionary<Node, Bool>()
        //UniqueElement = true
        //DuplicateElement = false
        for device in devices {
            processNode(&dictionary, node: device.positiveNode)
            processNode(&dictionary, node: device.negativeNode)
        }
        var list = [Node]()
        for (node, notDuplicate) in dictionary {
            if notDuplicate {
                list.append(node)
            }
        }
        return list
    }
    
    class func unionNodes(devices: [Device]) -> [Node] {
        var set = Set<Node>()
        for device in devices {
            set.add(device.negativeNode)
            set.add(device.positiveNode)
        }
        return set.array
    }
    
    private class func processNode(inout dictionary: Dictionary<Node, Bool>, node: Node) {
        if dictionary[node] != nil {
            dictionary[node] = false
        } else {
            dictionary[node] = true
        }
    }
    
    override var description: String {
        get {
            return "\(super.description): +\(self.positiveNode.safeName), -\(self.negativeNode.safeName)"
        }
    }
    
    func otherNode(node: Node) -> Node {
        assert(self.positiveNode != self.negativeNode, "A device is valid if it's nodes are the same; however, the otherNode function does not make sense in this case")
        return node == self.positiveNode ? self.negativeNode : self.positiveNode
    }
    
    // Returns the number of nodes replaced
    func replace(oldNode: Node, newNode: Node) -> Int {
        var count = 0
        if self.positiveNode == oldNode {
            self.positiveNode = newNode
            count++
        }
        // Do NOT optimize this with an else since device could have the same node for both terminals.
        if self.negativeNode == oldNode {
            self.negativeNode = newNode
            count++
        }
        return count
    }
    
    func synchronize(circuit: Circuit) -> Device {
        if let safeDevice = circuit.devices.get(self) {
            return safeDevice
        }
        assert(false, "Synchronize Failed: \(circuit.description) Finding: \(self.description)")
        return self
    }
    
}


