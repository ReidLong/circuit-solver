//
//  VoltageSource.swift
//  Circuit Solver
//
//  Created by Reid Long on 10/20/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import Foundation

class VoltageSource : Source {
    
    override init(positiveTerminal: Node, negativeTerminal: Node) {
        super.init(positiveTerminal: positiveTerminal, negativeTerminal: negativeTerminal);
    }
    
    override var description: String {
        return "Voltage Source: " + super.description
    }
    override func clone(positiveNode: Node, negativeNode: Node) -> Device {
        let newVoltageSource = VoltageSource(positiveTerminal: positiveNode, negativeTerminal: negativeNode)
        CircuitElement.cloneData(self, newElement: newVoltageSource)
        return newVoltageSource
    }
    
    override func suppress(inCircuit circuit: Circuit) {
        assert(circuit.devices.contains(self), "Can't remove self from circuit")
        let mergedNode = Node.mergeNode(self.negativeNode, templateNode: self.positiveNode)
        circuit.add(mergedNode)
        assert(circuit.contains(mergedNode), "This should be trivially true")
        
        self.positiveNode.replace(mergedNode)
        self.negativeNode.replace(mergedNode)
        
        // This does not hold
        assert(self.positiveNode != mergedNode && self.negativeNode != mergedNode, "Is this true?")
        
        circuit.remove(self.positiveNode, self.negativeNode)
        assert(!circuit.contains(self.positiveNode) && !circuit.contains(self.negativeNode), "This should be trivially true")

        circuit.removeDevice(self)
        assert(!circuit.contains(self.positiveNode) && !circuit.contains(self.negativeNode), "This should be trivially true")
        
        println("Supressed Circuit: \(circuit.description)")
    }
}


