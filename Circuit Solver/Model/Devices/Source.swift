//
//  Source.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/6/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

// Abstract placeholder pending expansion of multiple kinds of sources (voltage/current)
class Source: Device {
    class func sources(devices: [Device]) -> [Source] {
        var sources = [Source]()
        for device in devices {
            if let source = device as? Source {
                sources.append(source)
            }
        }
        return sources
    }
    
    func suppress(#inCircuit: Circuit) {
        assert(false, "Remove Not Implemented for source")
    }
}