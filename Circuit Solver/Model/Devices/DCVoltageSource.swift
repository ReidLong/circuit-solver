//
//  DCVoltageSource.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/3/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

class DCVoltageSource : VoltageSource {
    var voltageDifference: Double;
    
    override var currentThrough: Double {
        get {
            if let current = self._currentThrough {
                return current
            } else {
                assert(false, "Unsolved Current")
                return 0
            }
        }
        set {
            self._currentThrough = newValue
        }
    }
    
    private var _currentThrough: Double? = nil
    
    
    
    init(voltage: Double, positiveTerminal: Node, negativeTerminal: Node) {
        self.voltageDifference = voltage
        super.init(positiveTerminal: positiveTerminal, negativeTerminal: negativeTerminal)
        // This assert does not hold because the nodes may not have the voltage solved for at this point
        //assert(self.voltageAcross == self.voltageDifference, "DC Voltage Source Check: \(self.voltageAcross) != \(self.voltageDifference)")
        
    }
    override func clone(positiveNode: Node, negativeNode: Node) -> Device {
        let newDCVoltageSource = DCVoltageSource(voltage: self.voltageDifference, positiveTerminal: positiveNode, negativeTerminal: negativeNode)
        CircuitElement.cloneData(self, newElement: newDCVoltageSource)
        return newDCVoltageSource
    }
}