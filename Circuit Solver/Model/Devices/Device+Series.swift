//
//  Device+Series.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/5/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

extension Device {
    
    class func collapseSeries(devices: [Device]) -> Device {
        let unique = uniqueNodes(devices)
        assert(unique.count == 2)
        var deviceSingleton = devices[0]
        var compoundName = deviceSingleton.safeName
        
        for index in 1..<devices.count {
            deviceSingleton = (deviceSingleton as Resistor) + (devices[index] as Resistor)
            compoundName += " - \(devices[index].safeName)"
        }
        
        deviceSingleton.positiveNode = unique[0]
        deviceSingleton.negativeNode = unique[1]
        
        
        return deviceSingleton
    }
    
    func series(devicePartner: Device) -> Device {
        assert(false, "Fatal Collapse Error Not Implemented Exception")
        return self
    }
}

func +<T: Device>(lhs: T, rhs: T) -> T {
    return lhs.series(rhs) as T
}