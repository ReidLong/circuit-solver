//
//  CircuitTraversal.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/5/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

class CircuitTraversal {
    
    let nodes: (A: Node, B: Node)
    
    convenience init(_ nodeA: Node, _ nodeB: Node) {
        self.init(nodes: (nodeA, nodeB))
    }
    init(nodes: (Node, Node)) {
        self.nodes = nodes
    }
    
    var deepDevicesBetween: [Device] {
        get {
            if let safePath = self.pathBetween {
                return CircuitTraversal.allDevicesOn(path: safePath)
            } else {
                return [] // No devices exist on an incomplete path
            }
        }
    }
    
    var shallowDevicesBetween: [Device] {
        get {
           return Node.sharedDevices(self.nodes)
        }
    }
    
    // Returns the pairs of nodes between to faciliate directed traversal
    var pathBetween: [(Node, Node)]? {
        get {
            return CircuitTraversal.pathBetween(nodes: self.nodes)
        }
    }
    
    private class func allDevicesOn(#path: [(Node, Node)]) -> [Device] {
        var devices = Set<Device>() // Using a set here because I'm not sure if I'll have duplicates
        for nodePair in path {
            devices += Set<Device>(Node.sharedDevices(nodePair)) // That sick inline creation
        }
        return devices.array
    }
    
    private class func pathBetween(#nodes: (Node, Node)) -> [(Node, Node)]? {
        if nodes.0 == nodes.1 {
            return [nodes] // Handling helper requirement
        }
        
        if let rawList = CircuitTraversal.pathBetweenDFS(nodes.0, nodes.1) {
            return TupleUtilities.symmetricDuplicateRemoval(rawList)
        }
        
        println("Path Discovering Failed")
        return nil // Path discovering Failed
    }
    
    // Requires: current != end on first iteration
    private class func pathBetweenDFS(current: Node, _ end: Node, var visited: [Node] = [Node]()) -> [(Node, Node)]? {
        if current == end {
            return [(current, end)]
        }
        
        if visited.contains(current) {
            return nil
        }
        visited.append(current)
        
        var allSubPaths = [(Node, Node)]()
        
        for device in current.devices {
            let otherNode = current == device.negativeNode ? device.positiveNode : device.negativeNode
            
            if let subPaths = pathBetweenDFS(otherNode, end, visited: visited) {
                for nodePair in subPaths {
                    if nodePair.0 == nodePair.1 {
                        // This should be the base condition for solving the path
                        println("Path Destination Reached")
                        continue
                    }
                    allSubPaths.append(nodePair)
                }
                allSubPaths.append((current, otherNode)) // Why does this need to be here?
            }
        }
        
        visited.removeLast()
        
        if allSubPaths.count == 0 {
            return nil
        } else {
            return allSubPaths
        }
    }
    
    
}