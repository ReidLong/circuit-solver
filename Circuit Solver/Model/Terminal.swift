//
//  Terminal.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/28/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import Foundation

// Positive terminal maps to startPoint, negativeTerminal maps to endPoint on directed elements
enum Terminal {
    case Positive, Negative
    
    static func enumerate() -> [Terminal] {
        return [.Positive, .Negative]
    }
}

// Ya, this is a pretty empty file

