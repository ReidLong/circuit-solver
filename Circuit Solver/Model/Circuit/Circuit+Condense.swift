//
//  Circuit+Condense.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/6/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

extension Circuit {
    
    func removeExtraneousDevices(nodeA: Node, nodeB: Node) {
        var dictionary = Circuit.seedRemovalDictionary(self.devices)
        
        var dictionaryReference = DictionaryReference(dictionary: dictionary)
        
        Circuit.removeExtraneousDevices(nodeA, nodeB: nodeB, visited: [Node](), dictionaryReference: dictionaryReference)
        
        self.checkedRemove(dictionaryReference.dictionary)
    }
    
    private func checkedRemove(deviceCheck: [Device: Bool]) {
        for (device, checked) in deviceCheck {
            if !checked {
                self.removeDevice(device)
            }
        }
    }
    
    private class func seedRemovalDictionary(devices: Set<Device>) -> [Device: Bool] {
        var dictionary = [Device: Bool]()
        for device in devices {
            dictionary[device] = false
        }
        return dictionary
    }
    
    private class func removeExtraneousDevices(nodeA: Node, nodeB: Node, var visited: [Node], var dictionaryReference: DictionaryReference<Device, Bool>) -> Bool {
        if nodeA == nodeB {
            return true
        }
        if visited.contains(nodeA) {
            return false
        }
        visited.append(nodeA)
        var foundPath = false
        for device in nodeA.devices {
            let otherNode = device.otherNode(nodeA)
            if Circuit.removeExtraneousDevices(otherNode, nodeB: nodeB, visited: visited, dictionaryReference: dictionaryReference) {
                dictionaryReference.dictionary[device] = true
                foundPath = true
            }
        }
        visited.removeLast() // This line may not be needed since arrays are structs past by value 
        return foundPath
    }
    
}