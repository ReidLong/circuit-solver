//
//  Circuit+Collapse.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/5/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

extension Circuit {

    func safeCollapseParallel(#nodeA: Node, nodeB: Node) -> Bool {
        return false
    }
    
    func collapseParallel(#nodeA: Node, nodeB: Node) -> Bool {
        assert(self.contains(nodeA) && self.contains(nodeB), "Nodes not members of circuit A: \(self.contains(nodeA)) B: \(self.contains(nodeB))")
        let devicesBetween = CircuitTraversal(nodeA, nodeB).shallowDevicesBetween
        assert(devicesBetween.count >= 1, "How are these nodes in parallel without devices between them?")
        
        if devicesBetween.count == 1 {
            return false // Don't waste time and create bugs by trying to do fancy parallel logic if it isn't needed.
        }
        
        println("Attempting to collapse Parallel: \(nodeA.description) || \(nodeB.description) Devices: \(devicesBetween)")
        assert(CircuitElement.sameConcreteType(devicesBetween), "Elements Not The Same")
        
        let parallelDevice = Device.collapseParallel(devicesBetween)
        
        self.swap(list: devicesBetween, forItem: parallelDevice)
        
        return true
    }
    
    func safeCollapseSeries(node: Node) -> Bool {
        if node.devices.count == 2 {
            return self.collapseSeries(node)
        }
        return false
    }
    
    func collapseSeries(node: Node) -> Bool {
        assert(self.contains(node), "Node not member of circuit")
        assert(node.devices.count == 2, "Node must have two devices to be in series")
        let devices = node.devices.array
        println("Attemping to collapse Series: \(node.description) Devices: \(devices)")
        let seriesDevice = Device.collapseSeries(devices)
        
        self.swap(list: devices, forItem: seriesDevice)
        self.nodes.remove(node)
        
        return true
    }
    
    private func swap(#list: [Device], forItem item: Device) {
        // This is an inherently dangerous pattern
        assert(!list.contains(item), "This is a logic breaking error")
        self.removeDevices(list)
        self.addDevice(item)
    }
}