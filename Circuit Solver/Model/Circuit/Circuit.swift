//
//  Circuit.swift
//  Circuit Solver
//
//  Created by Reid Long on 10/22/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import Foundation

class Circuit : NSObject, Printable, DebugPrintable {
    var nodes: Set<Node>;
    var devices: Set<Device>;
    
    // MARK: Intitialization
    override convenience init() {
        self.init(currentNodes: [], currentDevices: [])
    }
    
    init(currentNodes: Set<Node>, currentDevices: Set<Device>) {
        self.nodes = currentNodes;
        self.devices = currentDevices;
        super.init()
    }
    
    convenience init(nodeArray: [Node], deviceArray: [Device]) {
        self.init(currentNodes: Set(nodeArray), currentDevices: Set(deviceArray))
    }
    
    convenience init(devices: Device...) {
        self.init()
        for device in devices {
            self.addDevice(device)
        }
    }
    
    func addDevice(device: Device) {
        assert(device.positiveNode.devices.contains(device) && device.negativeNode.devices.contains(device), "This should be trivially true unless the device model has been violated")
        self.devices += [device];
        self.nodes += [device.positiveNode, device.negativeNode];
    }
    
    func addDevices(devices: [Device]) {
        for device in devices {
            self.addDevice(device)
        }
    }

    func removeDevice(device: Device) {
        self.devices.remove(device)
        device.positiveNode.devices.remove(device)
        device.negativeNode.devices.remove(device)
    }
    
    func removeDevices(devices: [Device]) {
        for device in devices {
            self.removeDevice(device)
        }
    }
    
    func contains(node: Node) -> Bool {
        return self.nodes.contains(node)
    }
    
    private func removeNode(node: Node) {
        self.nodes.remove(node)
        // It might be nice to assert that none of the devices in the circuit reference this node
    }
    
    func remove(nodes: Node...) {
        for node in nodes {
            self.removeNode(node)
        }
    }
    
    func add(node: Node) {
        let expectedNil = self.nodes.add(node)
        assert(expectedNil == nil, "Overwriting Node (is this on purpose?)")
    }
    
    // MARK: Printable Protocol
    override var description: String {
        get {
            var formattedOutput = "Circuit:\n"
            formattedOutput += Device.format(self.devices, seperator: "\n", prefix: "Devices:\n")
            formattedOutput += Node.format(self.nodes)
            return formattedOutput
        }
    }
    
    // Mark: DebugPrintable Procotol
    override var debugDescription: String {
        get {
            return self.description
        }
    }
    
    var sources: [Source] {
        get {
            return Source.sources(self.devices.array)
        }
    }
    
    var isMinCircuit: Bool {
        get {
            if self.devices.count == 2 {
                let devices = self.devices.array
                if devices[0].isKindOfClass(Resistor) && devices[1].isKindOfClass(Source) {
                    return true
                }
                if devices[1].isKindOfClass(Resistor) && devices[0].isKindOfClass(Source) {
                    return true
                }
            }
            return false
        }
    }
    
    var minElements: (source: Source, resistor: Resistor) {
        get {
            assert(self.isMinCircuit)
            let devices = self.devices.array
            let source = Circuit.pick(Source.self, options: devices) as Source
            let resistor = Circuit.pick(Resistor.self, options: devices) as Resistor
            
            return (source, resistor)
        }
    }
    
    // TODO: Refactor
    class func pick(type: AnyClass, options: [Device]) -> Device {
        for element in options {
            if element.isKindOfClass(type) {
                return element
            }
        }
        assert(false, "Type Not Found")
        return options.first!
    }
}





