//
//  CircuitMutator.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/8/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

class CircuitMutator {
    let circuit: Circuit
    
    init(circuit: Circuit) {
        self.circuit = circuit
    }
    
    private var workingCircuit: Circuit {
        get {
            return self.circuit.clone()
        }
    }
    
    var circuitNoSources: Circuit {
        get {
            return CircuitMutator.removeSources(self.workingCircuit)
        }
    }
    
    // Used for super position calculations
    var subCircuits: [Circuit] {
        get {
            return CircuitMutator.subCircuit(self.workingCircuit)
        }
    }
    
    private class func removeSources(circuit: Circuit) -> Circuit {
        let sources = circuit.sources
        for source in sources {
            source.suppress(inCircuit: circuit)
        }
        return circuit
    }
    
    private class func subCircuit(circuit: Circuit) -> [Circuit] {
        let sources = circuit.sources
        var circuits = [Circuit]()
        for source in sources {
            var tempCircuit = circuit.clone()
            let syncedSource = source.synchronize(tempCircuit)
            for otherSource in sources {
                if syncedSource != otherSource {
                    let syncedOtherSource = otherSource.synchronize(tempCircuit) as Source
                    syncedOtherSource.suppress(inCircuit: tempCircuit)
                }
            }
            
            circuits.append(tempCircuit)
        }
        
        return circuits
    }
    
    
}