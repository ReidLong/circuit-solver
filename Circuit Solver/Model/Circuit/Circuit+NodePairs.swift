//
//  Circuit+NodePairs.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/11/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

extension Circuit {
    var nodePairs: [(Node, Node)] {
            return Circuit.generateNodePairs(self.devices)
    }
    
    private class func generateNodePairs(devices: Set<Device>) -> [(Node, Node)] {
        var cleaner = TupleDictionary<Node>()
        for device in devices {
            cleaner.add(device.nodes)
        }
        return cleaner.array
    }
}