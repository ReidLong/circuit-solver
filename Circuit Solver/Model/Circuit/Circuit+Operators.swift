//
//  Circuit+Operators.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/4/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

// Operators to be used on Circuit instances.  
// These are purely convenience operators and function solely as function shorthand. 
// Operators should not contain autonomous code

func +=<T: Device>(inout lhs: Circuit, rhs: T) {
    lhs.addDevice(rhs)
}

func +=<T: Device>(inout lhs: Circuit, rhs: [T]) {
    lhs.addDevices(rhs)
}