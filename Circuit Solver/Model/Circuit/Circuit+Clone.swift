//
//  Circuit+Clone.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/6/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

extension Circuit {

    func clone() -> Circuit {
        var clonedNodes: [Node:Node] = Node.cloneNodes(self.nodes.array)
        println("Cloned Nodes: \(clonedNodes.description)")
        var clonedDevices: [Device] = []
        for device in self.devices {
            // Using a continue - assert pattern to catch both else cases
            if let positiveNode = clonedNodes[device.positiveNode] {
                if let negativeNode = clonedNodes[device.negativeNode] {
                    let clonedDevice = device.clone(positiveNode, negativeNode: negativeNode)
                    clonedDevices += [clonedDevice]
                    continue
                }
            }
            assert(false, "Device Malformed \(device.description)")
        }
        let clonedCircuit = Circuit(nodeArray: clonedNodes.values.array, deviceArray: clonedDevices)
        return clonedCircuit
    }
}