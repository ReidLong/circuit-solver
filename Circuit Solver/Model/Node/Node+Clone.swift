//
//  Node+Clone.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/7/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

extension Node {
    
    func clone() -> Node {
        let clonedNode = Node()
        clonedNode.voltage = self.voltage;
        clonedNode.devices = []
        CircuitElement.cloneData(self, newElement: clonedNode)
        return clonedNode
    }
    
    class func cloneNodes(nodes: [Node]) -> [Node:Node] {
        var clonedNodes: [Node:Node] = [:]
        for node in nodes {
            let newNode = node.clone()
            clonedNodes[node] = newNode
            
        }
        return clonedNodes;
    }

}