
//
//  Node.swift
//  Circuit Solver
//
//  Created by Reid Long on 10/20/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import Foundation
 
// Current usage of NilLiteralConvertible is behaving like an optional
class Node : CircuitElement {
    
    
    var voltage: Double? = nil //Unsolved Voltage
    var devices: Set<Device> = Set()
    
    override var name: String? {
        didSet {
            if let safeOldValue = oldValue {
                if self.name == nil || self.name! != safeOldValue {
                    self.returnName(safeOldValue)
                }
            }
        }
    }
    
    var voltageString: String {
        get {
            if let voltageValue = self.voltage {
                return "\(voltageValue)V"
            } else {
                return "Unsolved"
            }
        }
    }
    
    override init() {
        super.init()
        self.name = "\(Node.defaultName)"
    }
    
    convenience init(voltage: Double) {
        self.init()
        self.voltage = voltage
    }
    init(name: String) {
        super.init()
        self.name = name
    }
    
    deinit {
        if let safeName = self.name {
            self.returnName(safeName)
        }
    }
    
    class func GROUND() -> Node {
        return Node(voltage: 0)
    }
    
    override func isEqualCircuitElement(otherCircuitElement: CircuitElement) -> Bool {
        if let otherNode = otherCircuitElement as? Node {
            return super.isEqualCircuitElement(otherCircuitElement)
        }
        return false
    }
    
    func synchronize(circuit: Circuit) -> Node {
        if let safeNode = circuit.nodes.get(self) {
            return safeNode
        }
        assert(false, "Syncrhonization Failed: \(circuit.description) Finding: \(self.description)")
        return self
    }

    
    
}