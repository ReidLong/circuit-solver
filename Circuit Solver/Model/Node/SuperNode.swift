//
//  SuperNode.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/11/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation


class SuperNode: Node {
    var hiddenNodes: [Node] = []
}