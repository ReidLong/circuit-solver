//
//  Node+Merge.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/30/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import Foundation

extension Node {
    // These names aren't reflective of the actual behavior
    class func mergeNode(oldNode: Node, templateNode: Node) -> Node {
        var newName: String = ""
        if let templateNodeName = templateNode.name {
            newName += "\(templateNodeName)-"
        }
        if let oldNodeName = oldNode.name {
            newName += "\(oldNodeName)"
        }
        
        
        var clonedNode = SuperNode(name: newName)
        clonedNode.devices += oldNode.devices
        clonedNode.devices += templateNode.devices
        clonedNode.hiddenNodes += [oldNode, templateNode]
        
        clonedNode.voltage = nil //Need to solve volage again after merging
        return clonedNode
    }
    
    // Merges the current node into the superNode
    func merge(superNode: Node) {
        for device in self.devices {
            if device.negativeNode == self {
                device.negativeNode = superNode
            }
            // No else to handle case where the device is insignificant to the circuit (self loop)
            if device.positiveNode == self {
                device.positiveNode = superNode
            }
        }
    }
}