//
//  Node+DefaultName.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/29/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import Foundation

let NODE_NAME_PATTERN = "^([A-Z])(\\1)*$" // I ❤️ RegEx

// This is a work around to add support for a static default name for nodes. 
// Used in the Node class initializers for generating unique names on device creation.
extension Node {
    // Internal struct to create a private static variable
    private struct InternalNodeStruct {
        // The number of letters in the longest name
        static var depth: Int = 0
        
        static var nameQueue = PriorityQueue<String>(higherPriority: <) // Super sick closure code passing the < opperator as the priority operator
        
        
    }
    
    // Used for system optimization.
    class func seedNames() {
        var queue = InternalNodeStruct.nameQueue
        if queue.isEmpty {
            
            assert(InternalNodeStruct.depth <= 1, "The depth should never reasonablly reach higher than 1 (lots and lots of elements)")
            let generatedValues = NameGenerator(type: NameType.Alphabetic(InternalNodeStruct.depth), prefix: "").names
            println("Generated Data: \(generatedValues.description)")
            queue.add(generatedValues)
            InternalNodeStruct.depth++
        }
    }
    
    // Simulates a class variable
    class var defaultName: String {
        get {
            seedNames()
            
            var queue = InternalNodeStruct.nameQueue
            
            assert(!queue.isEmpty, "Seeded queue is still empty?")
            
            let rawOutput = queue.remove()
            
            if let output = rawOutput {
                return output
            } else {
                assert(false, "The queue should have been refilled already")
                return "<Error>"
            }
        }
    }
    
    func returnName(name: String) {
        // Check is the name matches one of the system generated names
        if name.matches(regex: NODE_NAME_PATTERN) {
            // If matches return to the name queue for reuse
            var queue = InternalNodeStruct.nameQueue
            queue.add(name)
            
        }
        
    }
    
}