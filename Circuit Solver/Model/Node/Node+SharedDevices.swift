//
//  Node+SharedDevices.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/5/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

extension Node {
    func sharedDevices(otherNode: Node) -> [Device] {
        var devices: [Device] = []
        for device in self.devices {
            if TupleUtilities.symmetricEquality(device.nodes, (self, otherNode)) {
                devices.append(device)
            }
        }
        return devices
    }
    
    class func sharedDevices(nodePair: (A: Node, B: Node)) -> [Device] {
        return nodePair.A.sharedDevices(nodePair.B)
    }
}