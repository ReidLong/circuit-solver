//
//  Node+Format.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/4/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

let NODE_DEVICE_SEPERATOR = ", "

extension Node {
    class func format(nodes: Set<Node>) -> String {
        var formattedOutput = "Nodes:\n"
        for node in nodes {
            var deviceList = Device.format(node.devices, seperator: NODE_DEVICE_SEPERATOR, prefix: "")
            // Remove the last seperator from the deviceList
            if deviceList.isEmpty {
                deviceList = "<No Devices>"
            } else {
                // Device should have at least a few characters and one seperator
                assert(deviceList.utf16Count > NODE_DEVICE_SEPERATOR.utf16Count, "What the heck is happening in the Device.format function? \(deviceList.utf16Count)")
                deviceList = deviceList.substringToIndex(advance(deviceList.endIndex, -NODE_DEVICE_SEPERATOR.utf16Count))
            }
            formattedOutput += "{\(node.description): \(deviceList)}\n"
        }
        return formattedOutput
    }
}