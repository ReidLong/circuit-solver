//
//  Node+Replace.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/6/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

extension Node {
    func replace(newNode: Node) {
        assert(self != newNode, "Why are you replacing a node with itself?")
        for device in self.devices {
            assert(device.negativeNode == self || device.positiveNode == self, "This should be a trivially true check")
            device.replace(self, newNode: newNode)
        }
    }

}