//
//  CircuitSolver.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/3/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

// All concrete CircuitSolver sub-classes should implement some version of the "solve" function which effectively is the executable function to initate the solving behavior
class CircuitSolver {
    
    var circuit: Circuit
    
    init(circuit: Circuit) {
        self.circuit = circuit
    }
    
    // By default, checks the entire circuit for validity
    var isValid: Bool {
        get {
            return CircuitSolver.validate(self.relaventDevices())
        }
    }
    
    // By default will return all devics in circuit
    func relaventDevices() -> [Device] {
        return self.circuit.devices.array
    }

    class func validate(devices: [Device]) -> Bool {
        for device in devices {
            if !CircuitSolver.valid(device) {
                return false
            }
        }
        return true
    }
    
    // This should be a list of all concrete subclasses of Device which are valid to be included in a Circuit.
    // This list should always execute true and is designed only to catch bugs when new devices are added to the program.
    private class func valid(element: CircuitElement) -> Bool {
        return element.isKindOfClass(DCVoltageSource) || element.isKindOfClass(Resistor)
    }
    
}