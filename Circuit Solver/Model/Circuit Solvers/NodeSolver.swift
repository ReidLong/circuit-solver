//
//  NodeSolver.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/4/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

class NodeSolver: CircuitSolver {
    
    // True if successfully solved, false otherwise
    // Modifies the instance circuit's values to a correctly solved values if true
    func solve() -> Bool {
        
        let circuits = CircuitMutator(circuit: self.circuit).subCircuits
        for simpleCircuit in circuits {
            // simpleCircuit should contain only one source
            var circuitStack = NodeSolver.generateCircuitStack(simpleCircuit)
            assert(!circuitStack.isEmpty, "Needs to contain at least the simpleCircuit")
            
            let minCircuit = circuitStack.pop()
            // minCircuit should be a circuit with a single resistor and a single voltageSource
            assert(minCircuit.isMinCircuit)
            let minCircuitElements = minCircuit.minElements
            let voltageSource = minCircuitElements.source as DCVoltageSource // This is only true becasue the other sources have not been implemented
            let equivalentResistance = minCircuitElements.resistor
            voltageSource.negativeNode.voltage = 0
            voltageSource.positiveNode.voltage = voltageSource.voltageDifference
            // TODO: Check sign conventions here
            voltageSource.currentThrough = equivalentResistance.currentThrough
            
            var lastCircuit = minCircuit
            
            while !circuitStack.isEmpty {
                assert(NodeSolver.circuitNodesSolved(lastCircuit), "The last circuit needs to be solved to copy the data onto the new circuit")
                let currentCircuit = circuitStack.pop()
                NodeSolver.copySolvedNodes(solvedCircuit: lastCircuit, newCircuit: currentCircuit)
                let unsolvedNodes = NodeSolver.unsolvedNodes(currentCircuit)
                assert(unsolvedNodes.count <= 1, "Should only have one newly unsolved node")
                if unsolvedNodes.count == 1 {
                    // Undid a series collapse
                    // Perform voltage divider
                    var unsolvedNode = unsolvedNodes.first!
                    assert(unsolvedNode.devices.count == 2, "Not a series collapse")
                    let devices = unsolvedNode.devices.array
                    // TODO: Check for sign conventions
                    // These most definitely are not safe
                    let resistor1 = devices[0] as Resistor
                    let resistor2 = devices[1] as Resistor
                    let topVoltage = resistor1.otherNode(unsolvedNode).voltage!
                    let bottomVoltage = resistor2.otherNode(unsolvedNode).voltage!
                    
                    unsolvedNode.voltage = (topVoltage - bottomVoltage) * resistor2.resistance / (resistor1.resistance + resistor2.resistance) + bottomVoltage
                    
                } else {
                    // Undid a parallel collapse 
                    
                }
                assert(NodeSolver.circuitNodesSolved(currentCircuit), "Circuit Should be solved by now")
                lastCircuit = currentCircuit
                
            }
            
        }
        
        // Sum the solved node values of each of the circuits and plug in correct values into the final circuit
        
        for solvedSubCircuit in circuits {
            for node in solvedSubCircuit.nodes {
                if var trueNode = self.circuit.nodes.get(node) {
                    if trueNode.voltage == nil {
                        trueNode.voltage = 0 // Initialize nodes for solving
                    }
                    trueNode.voltage = trueNode.voltage! + node.voltage!
                }
            }
        }
        
        return true
    }
    
    
    
    class func generateCircuitStack(circuit: Circuit) -> Stack<Circuit> {
        // Do something simpler to the equivalent resistance calculations
        // Need to create some sort of system for backing the nodes that are shared. 
        var stack = Stack<Circuit>()
        stack.push(circuit)
        var depth: UInt = 0
        
        var workingCircuit = circuit.clone()
        
        do {
            let nodePairs = workingCircuit.nodePairs
            
            for (nodeA, nodeB) in nodePairs {
                // Do equivalent reistance stuff. 
                // Need to check if the nodes still exist in workingCircuit
                if workingCircuit.contains(nodeA) && workingCircuit.contains(nodeB) {
                    let syncedNodeA = nodeA.synchronize(workingCircuit)
                    let syncedNodeB = nodeB.synchronize(workingCircuit)
                    if workingCircuit.collapseParallel(nodeA: syncedNodeA, nodeB: syncedNodeB) {
                        // If the circuit was changed, push the new circuit onto the stack and then clone the circuit
                        stack.push(workingCircuit)
                        workingCircuit = workingCircuit.clone()
                    }
                }
                for seriesNode in [nodeA, nodeB] {
                    if workingCircuit.contains(seriesNode) {
                        let syncedSeriesNode = seriesNode.synchronize(workingCircuit)
                        if circuit.safeCollapseSeries(syncedSeriesNode) {
                            // If the circuit was changed, push the new circuit onto the stack and then clone the circuit
                            stack.push(workingCircuit)
                            workingCircuit = workingCircuit.clone()
                        }
                    }
                }
            }
            ++depth
            
            // The two devices desired at the end are a single VoltageSource and a single resistor
        } while (workingCircuit.devices.count > 2 && depth < MAX_DEPTH) // MAX_DEPTH defined in EquivalentResistanceClass
        
        assert(workingCircuit.isMinCircuit, "Circuit Should be a MinCircuit after stack completion")
        return stack
    }
    
    class func unsolvedNodes(circuit: Circuit) -> [Node] {
        var output = [Node]()
        for node in circuit.nodes {
            if node.voltage == nil {
                output.append(node)
            }
        }
        return output
    }
    
    class func circuitNodesSolved(circuit: Circuit) -> Bool {
        for node in circuit.nodes {
            if node.voltage == nil {
                return false
            }
        }
        return true
    }
    class func copySolvedNodes(#solvedCircuit: Circuit, newCircuit: Circuit) {
        for node in solvedCircuit.nodes {
            assert(node.voltage != nil, "Should be solved")
            NodeSolver.updateNode(node, circuit: newCircuit)
            if let superNode = node as? SuperNode {
                for dependentNode in superNode.hiddenNodes {
                    dependentNode.voltage = superNode.voltage
                    NodeSolver.updateNode(dependentNode, circuit: newCircuit)
                }
            }
        }
    }
    
    class func updateNode(node: Node, circuit: Circuit) {
        if var unsolvedCopy = circuit.nodes.get(node) {
            unsolvedCopy.voltage = node.voltage
        }
    }
    
    
}