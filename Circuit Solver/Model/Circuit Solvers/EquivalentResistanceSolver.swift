//
//  EquivalentResistanceSolver.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/4/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import Foundation

let MAX_DEPTH: UInt = 100000 // This should be sufficiently large.

class EquivalentResistanceSolver: CircuitSolver {
    
    let nodes: (A: Node, B: Node) // A fancy tuple to keep things interesting (and to resever the variable names nodeA and nodeB for later)
    
    init(circuit: Circuit, nodeA: Node, nodeB: Node) {
        self.nodes = (nodeA, nodeB)
        super.init(circuit: circuit)
    }
    
    
    override func relaventDevices() -> [Device] {
        // Should return a list of all devices between the parameter nodes (instance properties)
        let traversal = CircuitTraversal(nodes: self.nodes)
        return traversal.deepDevicesBetween
    }
    
    // Returns the equivalent resistance between these nodeA and nodeB
    func solve() -> Double {
        // if no valid path between nodes
        // return Double.infinity
        if self.nodes.A == self.nodes.B {
            return 0 // No resistance between two of the same nodes
        } else if CircuitTraversal(nodes: self.nodes).pathBetween == nil { // No valid path between nodes
            
            return Double.infinity
        } else {
            // All edge cases should be caught now
            return self.safeCalculate()
        }
    }
    
    private func safeCalculate() -> Double {
        let cleanCircuit = CircuitMutator(circuit: self.circuit).circuitNoSources
        let nodeA = cleanCircuit.nodes.get(self.nodes.A) // Synchronizing
        let nodeB = cleanCircuit.nodes.get(self.nodes.B) // Synchronizing
        assert(nodeA != nil && nodeB != nil, "What the heck happened in the clone?")
        
        let output = EquivalentResistanceSolver.calculateEquivalentResistance(nodeA: nodeA!, nodeB: nodeB!, circuit: cleanCircuit)
        return output
    }
    
    private class func calculateEquivalentResistance(#nodeA: Node, nodeB: Node, var circuit: Circuit) -> Double {
        assert(circuit.contains(nodeA) && circuit.nodes.get(nodeA)! === nodeA, "Should be same reference")
        assert(circuit.contains(nodeB) && circuit.nodes.get(nodeB)! === nodeB, "Should be same reference")
        
        println("Original Circuit:\(circuit.description)")
        circuit.removeExtraneousDevices(nodeA, nodeB: nodeB) // Clean out the crap 
        println("Condensed Circuit: \(circuit.description)")
        var depth: UInt = 0
        do {
            println("Loop \(depth) Circuit: \(circuit.description)")
            let path = CircuitTraversal(nodeA, nodeB).pathBetween! // There must be a path between the nodes for this function to be called. This is a safe unwrap
            println("Path between \(nodeA.description) and \(nodeB.description): \(path)")
            
            for (leftNode, rightNode) in path {
                
                // Need to check if the circuit still contains these nodes because the path isn't updated when nodes are collapsed out of existance
                if circuit.contains(leftNode) && circuit.contains(rightNode) {
                    circuit.collapseParallel(nodeA: leftNode, nodeB: rightNode)

                }
                
                for seriesNode in [leftNode, rightNode] { // My cmd+c, cmd+v is broken...
                    if circuit.contains(seriesNode) && seriesNode != nodeA && seriesNode != nodeB {
                    circuit.safeCollapseSeries(seriesNode)

                    }
                }
        
            }
            
            depth++
        } while (circuit.devices.count > 1 &&  depth < MAX_DEPTH)
        
        println("Final Circuit: \(circuit.description)")
        let devicesBetweenEndPoints = CircuitTraversal(nodeA, nodeB).shallowDevicesBetween
        
        assert(devicesBetweenEndPoints.count == 1, "There should only be one device left in the circuit")
        
        let singleResistor = devicesBetweenEndPoints.first! as Resistor
        
        return singleResistor.resistance
    
    }
    
}