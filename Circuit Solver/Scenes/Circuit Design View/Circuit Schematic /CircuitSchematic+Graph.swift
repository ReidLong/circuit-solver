//
//  CircuitSchematic+Graph.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/29/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

extension CircuitSchematic {
    
    var circuitGraph: CircuitGraph {
        get {
            var graph = CircuitGraph(list: self.elements)
            return graph
        }
    }
  
}
