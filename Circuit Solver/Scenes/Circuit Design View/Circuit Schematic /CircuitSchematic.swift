//
//  CircuitSchematic.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/18/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

protocol CircuitSchematicDelegate {
    func drawHelper(rect: CGRect)
}

class CircuitSchematic: UIView, WireDelegate {
    
    var elements: [UICircuitElement] = []
    
    var delegate: CircuitSchematicDelegate? = nil
    
    func clearDeletedElements() {
        var deleted: [UICircuitElement] = []
        for device in self.elements {
            if device.deleted {
                deleted.append(device)
            }
        }
        for device in deleted {
            self.removeDevice(device)
        }
        self.setNeedsDisplay()
        
    }
    
    
    func extractCircuit() -> Circuit {
        var circuit = Circuit()
        for element in self.elements {
            if let uiDevice = element as? UIDevice {
                circuit += uiDevice.device
            }
        }
        
        println(circuit.description)
        return circuit
    }
    
    
    
    func addDevice(device: UICircuitElement) {
        // Special behavior for merging nodes is handled in the Wire setNode:terminal function
        self.elements.append(device)
        self.setNeedsDisplay()
    }

    func removeDevice(device: UICircuitElement) {
        var targetIndex = -1
        for index in 0..<self.elements.count {
            if self.elements[index] == device {
                targetIndex = index
                break
            }
        }
        assert(0 <= targetIndex, "Device Not Found")
        let removedElement = self.elements.removeAtIndex(targetIndex)
        
        if let removedDevice = removedElement as? UIDevice {
            println("Processing Special Removal for UIDevices")
            removedDevice.processRemoval()
        }
        // Only need to do special processing if a wire is removed
        if let removedWire = removedElement as? Wire {
            let circuitGraph = self.circuitGraph
            removedWire.processRemoval(circuitGraph)
        }
        
        self.setNeedsDisplay()
    }
    
    func allWires(node: Node) -> [Wire] {
        var list = [Wire]()
        for element in self.elements {
            if let wire = element as? Wire {
                if let wireNode = wire.node {
                    if wireNode == node {
                        list.append(wire)
                    }
                }
            }
        }
        return list
    }
    

    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        for device in self.elements {
            device.draw()
        }
        
        
        if let callableDelegate = self.delegate {
            callableDelegate.drawHelper(rect)
        }
        
    }

    
    
}
