//
//  CircuitViewController.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/18/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

// MARK: Constants
let DEVICE_PICKER_FONT = "Helvetica Neue"
let DEVICE_PICKER_FONT_SIZE = CGFloat(20)

let DEVICE_PICKER_ROW_HEIGHT = CGFloat(36) //Constant for curvature
let DEVICE_PICKER_FONT_COLOR = UIColor.whiteColor()

let MODE_CONTROL_BUILD_INDEX = 0
let MODE_CONTROL_ANALYZE_INDEX = 2
let MODE_CONTROL_VIEW_INDEX = 1

let INSPECTOR_VIEW_CONTROLLER_IDENTIFIER = "ElementInspector"

enum DeviceType: Int {
    case Resistor = 0
    case Ground = 1
    case StraightWire = 2
    case CurvedWire = 3
}

enum MeasuringDeviceType: Int {
    case Ohmmeter = 0
}

let MAX_ZOOM_SCALE: CGFloat = 1.25

enum CircuitViewControllerState {
    case Editing, Inspecting, Viewing
    func supportsEditing() -> Bool {
        switch self {
        case .Editing:
            fallthrough
        case .Inspecting:
            return true
        case .Viewing:
            return false
        }
    }

}

class CircuitViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UIScrollViewDelegate, UICircuitElementDelegate, CircuitSchematicDelegate {
    @IBOutlet weak var modeControl: UISegmentedControl!
    @IBOutlet weak var devicePicker: UIPickerView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var circuitSchematic: CircuitSchematic!
    
    //var gestureRecognizer: UIPanGestureRecognizer = UIPanGestureRecognizer()
    @IBOutlet var gestureRecognizer: UIPanGestureRecognizer!
    
    var state: CircuitViewControllerState = CircuitViewControllerState.Viewing {
        didSet {
            self.updateUIForState(self.state)
        }
    }
    
    let deviceOptions = ["Resistor", "Ground", "Straight Wire", "Curved Wire"]
    let measuringOptions = ["Ohmmeter"]
    
    var queuedCircuit: Circuit? = nil
    
    var currentDevice: UICircuitElement? = nil
    
    override func viewDidLoad() {

        self.circuitSchematic.delegate = self
        self.scrollView.addSubview(self.circuitSchematic)
        self.scrollView.contentSize = self.circuitSchematic.frame.size
        
        self.scrollView.bounces = true
        self.scrollView.delegate = self
        self.devicePicker.dataSource = self
        self.devicePicker.delegate = self
        
        let scrollViewFrame = self.scrollView.frame
        let scaleWidth = scrollViewFrame.size.width / self.scrollView.contentSize.width
        let scaleHeight = scrollViewFrame.size.height / self.scrollView.contentSize.height
        let minScale = min(scaleWidth, scaleHeight)
        println("Minimum Zoom Scale: \(minScale)")
        
        self.scrollView.minimumZoomScale = minScale
        self.scrollView.maximumZoomScale = MAX_ZOOM_SCALE
        self.scrollView.zoomScale = 1.0
        
        //self.gestureRecognizer = UIPanGestureRecognizer(target: self.circuitSchematic, action: Selector("panGestureHandler:"))
        
        self.modeControl.selectedSegmentIndex = MODE_CONTROL_VIEW_INDEX
        self.state = CircuitViewControllerState.Viewing
        
        Node.seedNames() // Optimization
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.circuitSchematic.clearDeletedElements()
        
    }
    
    
    
    @IBAction func tapGestureHandler(sender: UITapGestureRecognizer) {
        println("Double Tap Detected")
        let numberOfTouches = sender.numberOfTouches()
        if numberOfTouches > 0 {
            // Not neccessarily a hard failure, but I would be surpriesed if multiple double taps are reported.
            assert(numberOfTouches == 1, "There should only be one touch for double tap")
            let touchLocation = sender.locationOfTouch(0, inView: self.circuitSchematic)
            for device in self.circuitSchematic.elements {
                if device.didCollide(touchLocation) {
                    println("Collision with \(device.description)")
                    
                    if let measuringDevice = device as? MeasuringDevice {
                        //Always recalculating the circuit to prevent edge cased issues with deletion of elements - This could be fixed if the queuedCircuit is properly cached
                        self.queuedCircuit = self.circuitSchematic.extractCircuit()
                        assert(self.queuedCircuit != nil, "This should really never happen")
                        measuringDevice.updateCalculations(self.queuedCircuit!)
                    }
                    
                    let viewController = self.storyboard?.instantiateViewControllerWithIdentifier(INSPECTOR_VIEW_CONTROLLER_IDENTIFIER) as CircuitElementInspectorViewController
                    viewController.circuitElement = device
                    viewController.dataDictionary = device.generateInspectorData()
                    self.presentViewController(viewController, animated: true, completion: nil)
                    
                    //Shouldn't have multiple collisions. If the user does create multiple collisions there will be no layering order. To possibly support last drawn device = highest collision precedence (visual ordering), reverse implement the circuitschematic drawing function to return devices in the order that would reverse the drawing order
                    break
                }
            }
            
            
            
        } else {
            assert(false, "Why is the gesture reported when there are no touches?")
        }
        
    }
    
    
    @IBAction func panGestureHandler(sender: UIPanGestureRecognizer) {
        
        let possibleTouchLocation: CGPoint? = sender.numberOfTouches() > 0 ? sender.locationOfTouch(0, inView: self.circuitSchematic) : nil
        
        switch gestureRecognizer.state {
        case UIGestureRecognizerState.Began:
            println("New Gesture Started")
            if currentDevice != nil {
                println("Device Still Pending During Restart (safe failure)")
            }
            if let touchLocation = possibleTouchLocation {
                self.currentDevice = createCircuitElement(location: touchLocation)
            } else {
                assert(false, "Touch Missing on Device Began")
            }
            CircuitConnectionUtilities.snap(element: self.currentDevice!, toList: self.circuitSchematic.elements, direction: Terminal.Positive)
            
        case UIGestureRecognizerState.Changed:
            assert(self.currentDevice != nil, "Current Device State Invalid")
            if let touchLocation = possibleTouchLocation {
                self.currentDevice!.endPoint = touchLocation
            } else {
                assert(false, "Touch Missing on Device Update")
            }
        case UIGestureRecognizerState.Ended:
            println("Gesture Successful")
            assert(self.currentDevice != nil, "Current Device State Invalid")
            
            // Edge case to cover connecting ground node 
            if self.currentDevice!.canConnect(terminal: Terminal.Negative) {
                CircuitConnectionUtilities.snap(element: self.currentDevice!, toList: self.circuitSchematic.elements, direction: Terminal.Negative)
            }
            self.circuitSchematic.addDevice(self.currentDevice!)
            if let measuringDevice = self.currentDevice as? MeasuringDevice {
                // Possibly a dangerous upwrap here; This blind unwrap has not been verified as safe. (I'm basically guessing about the code right now)
                assert(self.queuedCircuit != nil, "Logical Error")
                measuringDevice.updateCalculations(self.queuedCircuit!)
            }
            self.currentDevice = nil
            
        default:
            self.currentDevice = nil
            println("Device Creation Cancelled")
        }
    }
    
    func createCircuitElement(location point: CGPoint) -> UICircuitElement? {
        if self.state == CircuitViewControllerState.Editing {
            return self.createDeviceFromSelectedIndex(location: point)
        } else if self.state == CircuitViewControllerState.Inspecting {
            return self.createMeasuringDeviceFromSelectedIndex(location: point)
        } else {
            return nil
        }
    }
    
    func createMeasuringDeviceFromSelectedIndex(location point: CGPoint) -> DirectedElement? {
        let selectedIndex = self.devicePicker.selectedRowInComponent(0)
        var device: DirectedElement? = nil
        
        if let measuringType = MeasuringDeviceType(rawValue: selectedIndex) {
            switch measuringType {
            case MeasuringDeviceType.Ohmmeter: 
                device = Ohmmeter(startPoint: point, endPoint: point)
                
            }
        } else {
            assert(false, "System not setup for measuring device")
        }
        
    
        device?.delegate = self
        
        return device
    
    }
    
    func createDeviceFromSelectedIndex(location point: CGPoint) -> UICircuitElement? {
        let selectedIndex = self.devicePicker.selectedRowInComponent(0)
        var device: UICircuitElement? = nil
        let deviceType = DeviceType(rawValue: selectedIndex)
        if let safeDeviceType = deviceType {
            switch safeDeviceType {
            case DeviceType.Resistor:
                let resistor = Resistor()
                device = UIResistor(positiveTerminalPoint: point, negativeTerminalPoint: point, resistor: resistor)
            case DeviceType.StraightWire:
                device = StraightWire(startPoint: point, endPoint: point, wireDelegate: self.circuitSchematic)
            case DeviceType.CurvedWire:
                device = CurvedWire(startPoint: point, endPoint: point, wireDelegate: self.circuitSchematic)
            case DeviceType.Ground:
                device = Ground(startPoint: point, endPoint: point)
            default:
                assert(false, "Circuit Not Setup for device")
            }
        
        }
        device?.delegate = self
        
        return device
        
    }
    
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }

    // MARK: Actions
    @IBAction func modeControlChanged(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == MODE_CONTROL_ANALYZE_INDEX {
            self.state = CircuitViewControllerState.Inspecting
            //self.updateMeasurementDevices() // Removed to consolidate logic in a single location for multi states.  Remove this comment in a later build
        } else if sender.selectedSegmentIndex == MODE_CONTROL_BUILD_INDEX {
           self.state = CircuitViewControllerState.Editing
        } else if sender.selectedSegmentIndex == MODE_CONTROL_VIEW_INDEX {
            self.state = CircuitViewControllerState.Viewing
        } else {
            assert(false, "Selected Mode Does Not Exist")
        }
        
    }
    
    func updateUIForState(state: CircuitViewControllerState) {
        let editingSupport = self.state.supportsEditing()
        println("Editing Support: \(editingSupport)")
        
        self.devicePicker.userInteractionEnabled = editingSupport
        self.scrollView.scrollEnabled = !editingSupport
        
        if editingSupport {
            self.circuitSchematic.addGestureRecognizer(self.gestureRecognizer)
        } else {
            self.circuitSchematic.removeGestureRecognizer(self.gestureRecognizer)
        }
        
        if self.state == CircuitViewControllerState.Inspecting {
            self.queuedCircuit = self.circuitSchematic.extractCircuit()
        } else {
            self.queuedCircuit = nil
        }
        
        self.devicePicker.reloadAllComponents()
    }
    
    func pickerList(forState state: CircuitViewControllerState) -> [String] {
        if self.state == CircuitViewControllerState.Editing {
            return self.deviceOptions
        } else if state == CircuitViewControllerState.Inspecting {
            return self.measuringOptions
        } else {
            return []
        }
    }
    
    
    // MARK: Delgates and data sources
    // MARK: Data Sources
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerList(forState: self.state).count
    }

    // MARK: Delegates
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        
        return self.pickerList(forState: self.state)[row]
    }
    

    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return DEVICE_PICKER_ROW_HEIGHT
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
        var pickerLabel = view as UILabel!
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel.textAlignment = NSTextAlignment.Center
        }
        let titleData = self.pickerView(pickerView, titleForRow: row, forComponent: component)
        let attributedTitle = NSAttributedString(string: titleData, attributes: CircuitViewController.devicePickerTextAttributes())
        pickerLabel.attributedText = attributedTitle
        
        return pickerLabel
    }
    class func devicePickerTextAttributes() -> [NSObject: AnyObject]? {
        return [NSFontAttributeName:UIFont(name: DEVICE_PICKER_FONT, size: DEVICE_PICKER_FONT_SIZE)!, NSForegroundColorAttributeName: DEVICE_PICKER_FONT_COLOR]
    }
    
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.circuitSchematic
    }
    
    func redrawNeeded() {
        self.circuitSchematic.setNeedsDisplay()
    }
    
    func drawHelper(rect: CGRect) {
        self.currentDevice?.draw()
    }
    
}
