//
//  CircuitInspectorCollectionViewCell.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/21/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

protocol CircuitElementInspectorCellDelegate {
    func valueChanged(cell: CircuitInspectorCollectionViewTextCell)
    func didGetFocus(cell: CircuitInspectorCollectionViewTextCell)
}

class CircuitInspectorCollectionViewTextCell: UICollectionViewCell, UITextFieldDelegate {
    @IBOutlet weak var fieldLabel: UILabel!
    
    @IBOutlet weak var inputLabel: UITextField!
    
    var delegate: CircuitElementInspectorCellDelegate? = nil
    
    var inspectorDataElement: InspectorData? = nil
    
    var indexPath: NSIndexPath? = nil
    
    var unit: String? = nil
    
    var safeUnit: String {
        get {
            if let unitString = self.unit {
                return unitString
            }
            // Treat no unit as empty string
            return ""
        }
    }
    
    func setup(inspectorData: InspectorData) {
        self.inspectorDataElement = inspectorData
        self.fieldLabel.text = inspectorData.fieldName
        self.inputLabel.text = inspectorData.fieldValue
        self.inputLabel.userInteractionEnabled = inspectorData.dataType.supportsEditing()
        
        if !self.inputLabel.userInteractionEnabled {
            // Add additional customization features for static text
            self.inputLabel.borderStyle = UITextBorderStyle.None
        }
        
        self.inputLabel.delegate = self
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    @IBAction func editingDidEndHandler(sender: UITextField) {
        println("Text Cell Update Called")
        self.valuesUpdated()
    }
    
    func valuesUpdated() {
        println("Trying to update values")
        if let safeDelegate = self.delegate {
            safeDelegate.valueChanged(self)
        }
    }
    
    func currentData() -> String {
        var rawData = self.inputLabel.text
        if rawData.hasSuffix(self.safeUnit) {
            rawData = rawData.substringToIndex(advance(rawData.endIndex, -1 * self.safeUnit.utf16Count))
        }
        return rawData
    }
    
    
    
}
