//
//  CircuitInspectorCollectionViewNumberCell.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/22/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

class CircuitInspectorCollectionViewNumberCell: CircuitInspectorCollectionViewTextCell, UITextFieldDelegate {
    
    @IBOutlet weak var dataSlider: UISlider!

    
    @IBAction func dataSliderValueChanged(sender: UISlider) {
        self.updateText()
    }
    
    @IBAction func textFieldValueChanged(sender: UITextField) {
        var text = sender.text
        if text.hasSuffix(self.safeUnit) {
            println("Removing Suffix: \(text)")
            text = text.substringToIndex(advance(text.endIndex, -countElements(self.safeUnit)))
            println("After: \(text)")
        }
        
        if let newValue = text.toFloat() {
            if self.dataSlider.value != newValue && self.dataSlider.minimumValue <= newValue && newValue <= self.dataSlider.maximumValue {
                self.dataSlider.value = newValue
            }
        }
        self.updateText()
    }
    
    func updateText() {
        self.inputLabel.text = "\(self.dataSlider.value.format())\(self.safeUnit)"
    }
    
    
    @IBAction func sliderTouchEnded(sender: UISlider) {
        self.valuesUpdated()
    }
    
}
