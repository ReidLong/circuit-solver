//
//  CircuitElementInspector.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/21/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

let DISMISS_BUTTON_TAG = 1
let DELETE_BUTTON_TAG = 2

let NUMBER_CELL_NAME = "NumberCell"
let TEXT_CELL_NAME = "TextCell"

let NUMBER_CELL_HEIGHT: CGFloat = 73
let TEXT_CELL_HEIGHT: CGFloat = 36


class CircuitElementInspectorViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, CircuitElementInspectorCellDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var collectionview: UICollectionView!
    
    @IBOutlet weak var deleteButton: UIButton!
    
    
    // MARK: Initialization Fields
    // These fields should be set during the segue preparation process.  No view controller should be displayed before these fields are set
    var circuitElement: UICircuitElement {
        get {
            if let safeCircuitElement = self._circuitElement {
                return safeCircuitElement
            }
            assert(false, "Malformed Inspector")
            return _circuitElement!
        }
        set {
            self._circuitElement = newValue
        }
    }
    
    private var _circuitElement: UICircuitElement? = nil
    
    var dataDictionary: [InspectorData] = []
    
    // End MARK
    
    var activeIndexPath: NSIndexPath? = nil

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    override func viewDidLoad() {
        //self.collectionview.delegate = self
        //self.collectionview.dataSource = self
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardDidShow:"), name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
        println("Did Load")
    }
    
    func keyboardDidShow(notification: NSNotification) {
        assert(notification.userInfo != nil, "User Info should not be null on this event")
        if let safeUserInfo = notification.userInfo {
            
            let info = safeUserInfo as Dictionary
            
            let rawKeyboardRect = (info[UIKeyboardFrameBeginUserInfoKey] as NSValue).CGRectValue()
            let keyboardRect = self.view.convertRect(rawKeyboardRect, fromView: nil)
            
            let contentInsets = UIEdgeInsetsMake(0, 0, keyboardRect.size.height, 0)
            self.collectionview.contentInset = contentInsets
            self.collectionview.scrollIndicatorInsets = contentInsets
            
            if let safeIndexPath = self.activeIndexPath {
                self.collectionview.scrollToItemAtIndexPath(safeIndexPath, atScrollPosition: UICollectionViewScrollPosition.CenteredVertically, animated: true)
            }
        }
        
        
    }
    
    func keyboardWillHide(notifcation: NSNotification) {
        let zeroInsets = UIEdgeInsetsZero
        self.collectionview.contentInset = zeroInsets
        self.collectionview.scrollIndicatorInsets = zeroInsets
    }
    
    func didGetFocus(cell: CircuitInspectorCollectionViewTextCell) {
       self.activeIndexPath = cell.indexPath
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataDictionary.count
    }
    
    func inspectorDataElement(forIndexPath indexPath: NSIndexPath) -> InspectorData {
        return dataDictionary[indexPath.item]
    }
    
    func getIndex(forKey key: String) -> Int {
        for index in 0..<self.dataDictionary.count {
            let dataElement = self.dataDictionary[index]
            if dataElement.fieldName == key {
                return index
            }
        }
        assert(false, "What the heck is going on here?")
        return -1
    }
    
    func valueChanged(cell: CircuitInspectorCollectionViewTextCell) {
        println("Value changed received")
        if var dataCopy = cell.inspectorDataElement {
            assert(dataCopy.dataType.supportsEditing(), "Editing Not Supported For Device")
            let index = self.getIndex(forKey: dataCopy.fieldName)
            dataCopy.fieldValue = cell.currentData()
            self.dataDictionary[index] = dataCopy
            if !self.circuitElement.updateModel(dataCopy) {
                assert(false, "Update Failed (Verify Update Chain)")
            }
        } else {
            assert(false, "Malconstructed Cell")
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let element = self.inspectorDataElement(forIndexPath: indexPath)
        var cell: CircuitInspectorCollectionViewTextCell? = nil
        switch element.dataType {
        case let DataInputType.Number(minValue, maxValue):
            let numberCell = collectionview.dequeueReusableCellWithReuseIdentifier(NUMBER_CELL_NAME, forIndexPath: indexPath) as CircuitInspectorCollectionViewNumberCell
            // Setting max first because of concerns about shifting the minValue above the calibrated max
            numberCell.dataSlider.maximumValue = Float(maxValue)
            numberCell.dataSlider.minimumValue = Float(minValue)
            if let value = element.fieldValue.toFloat() {
                numberCell.dataSlider.value = value
            } else {
                assert(false, "Type Mismatch in CircuitElementInspector")
            }
            cell = numberCell
        case DataInputType.Static:
            fallthrough
        case DataInputType.Text:
            cell = collectionview.dequeueReusableCellWithReuseIdentifier(TEXT_CELL_NAME, forIndexPath: indexPath) as? CircuitInspectorCollectionViewTextCell
        }
        
        if var safeCell = cell {
            safeCell.setup(element)
            safeCell.delegate = self
            return safeCell
        }
        
        
        assert(false, "Unsafe Unwrap")
        return cell!
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let element = self.inspectorDataElement(forIndexPath: indexPath)
        var height: CGFloat = -1
        switch element.dataType {
        case DataInputType.Number:
            height = NUMBER_CELL_HEIGHT
        case DataInputType.Static:
            fallthrough
        case DataInputType.Text:
            height = TEXT_CELL_HEIGHT
        }
        var size = CGSizeMake(self.view.frame.size.width, height)
        return size
    }
    
    
    
    
    @IBAction func buttonClicked(sender: UIButton) {
        switch sender.tag {
        case DELETE_BUTTON_TAG:
            self.deleteDevice()
            fallthrough
        case DISMISS_BUTTON_TAG:
            self.dismissInspector()
        default:
            assert(false, "Button Clicked is not configured properly. Tag: \(sender.tag)")
        }
    }
    
    func deleteDevice() {
        self.circuitElement.deleted = true
    }
    
    func dismissInspector() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}