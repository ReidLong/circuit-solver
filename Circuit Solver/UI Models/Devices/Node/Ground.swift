//
//  Ground.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/2/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import UIKit

let MAX_BAR_WIDTH: CGFloat = 30
let BAR_SEPERATION: CGFloat = 10
let BAR_COUNT = 3
let HIT_BOX_SCALE: CGFloat = 0.70

class Ground: UINode {
    
    override init(startPoint: CGPoint, endPoint: CGPoint, node: Node) {
        super.init(startPoint: startPoint, endPoint: endPoint, node: node)
        node.voltage = 0
    }
    
    convenience override init(startPoint: CGPoint, endPoint: CGPoint) {
        let dummyNode = Node(voltage: 0)
        self.init(startPoint: startPoint, endPoint: endPoint, node: dummyNode)
    }
    
    // Can only connect on postive terminal side (startPoint)
    override func canConnect(#terminal: Terminal) -> Bool {
        return terminal != Terminal.Negative
    }
    
    override var width: CGFloat {
        get {
            return MAX_BAR_WIDTH * HIT_BOX_SCALE
        }
    }
    
    override func redraw() {
        var bezierPath = UIBezierPath()
        
        
        var offsetVector = Vector.unitVector(CGPoint.angle(self.startPoint, self.endPoint))
        let deltaX = self.endPoint.x - self.startPoint.x
        let directionMultiplier = deltaX == 0 ? -1 : copysign(1, self.endPoint.x - self.startPoint.x)
        // Possibly need to adjust for zero case
        
        
        bezierPath.moveToPoint(self.startPoint)
        
        // Assuming X orientation
        let barEdge = CGFloat(BAR_COUNT - 1) * BAR_SEPERATION
        offsetVector.magnitude = barEdge
        let barCenterPoint = CGPointMake(self.endPoint.x - offsetVector.deltaX * directionMultiplier, self.endPoint.y - offsetVector.deltaY * directionMultiplier)
        bezierPath.addLineToPoint(barCenterPoint)
        
        
        
        let lengthDepreciation = MAX_BAR_WIDTH / CGFloat(BAR_COUNT)
        for barNumber in 0..<BAR_COUNT {
            
            let originalPoint = bezierPath.currentPoint
            
            let length = (MAX_BAR_WIDTH - CGFloat(barNumber) * lengthDepreciation) / 2
            offsetVector.magnitude = length
            let offsetPoints = originalPoint.offset(offsetVector)
            
            assert(offsetPoints.count == 2, "There should only be 2 points")
            bezierPath.moveToPoint(offsetPoints[0])
            bezierPath.addLineToPoint(offsetPoints[1])
            
            offsetVector.magnitude = BAR_SEPERATION
            let midPoint = CGPointMake(originalPoint.x + offsetVector.deltaX * directionMultiplier, originalPoint.y + offsetVector.deltaY * directionMultiplier)
            bezierPath.moveToPoint(midPoint) // Used for loop logic
        }
        
        UIColor.whiteColor().setStroke()
        bezierPath.stroke()
        
    }
    
    override func node(#terminal: Terminal) -> Node? {
        switch terminal {
        case Terminal.Positive:
            return self.node
        default:
            return nil
        }
    }
    
    override func setNode(#terminal: Terminal, node: Node) {
        assert(terminal == Terminal.Positive, "No Negative Terminal on Ground")
        self.node = node
        node.voltage = 0
    }
}
