//
//  StraightWire.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/27/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

class StraightWire: Wire {
    
    override func redraw() {
        self.drawWire()
    }
}
