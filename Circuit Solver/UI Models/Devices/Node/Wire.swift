//
//  Wire.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/28/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

protocol WireDelegate {
    func allWires(node: Node) -> [Wire]
}

class Wire: UINode {

    var wireDelegate: WireDelegate
    
    init(startPoint: CGPoint, endPoint: CGPoint, wireDelegate: WireDelegate) {
        self.wireDelegate = wireDelegate
        super.init(startPoint: startPoint, endPoint: endPoint)
    }
    
    override func node(terminal _: Terminal) -> Node? {
        return self.node
    }
    
    override func setNode(terminal _: Terminal, node: Node) {
        if self.node == nil {
            // Standard behavior for first connection
            self.node = node
        } else if self.node != node { // Super dangerous and super cool logic
            assert(self.node?.uniqueId != node.uniqueId, "This is a verification on the equality function")
            // Process the other node and replace all of its devices with the currently set node
            println("Merging Nodes On Wire")
            
            if let safeNode = self.node {
                node.merge(safeNode)
                // Also needs to determine all wires which referenced node and correct their nodes
                let connectedWires = self.wireDelegate.allWires(node)
                for wire in connectedWires {
                    wire.node = safeNode
                }
            } else {
                assert(false, "Logical Error in Wire.setNode:terminal")
            }
            

        } else {
            assert(self.node?.uniqueId == node.uniqueId, "Verification on equality function")
            // Nodes are the same so nothing needs to be done. 
            println("Double Setting Node in Wire")
        }
    }
    
    
}
