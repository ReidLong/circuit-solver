//
//  Wire+Removal.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/30/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

extension Wire {

    func processRemoval(graph: CircuitGraph) {
        if graph.path(fromPoint: self.startPoint, toPoint: self.endPoint) {
            // Don't have to do anything because this circuit contains multiple wires connecting the same devices. The wire removed was extraneous
            println("Duplicate Wire Removed")
        } else {
            assert(self.node != nil, "How can a wire's node be nil on removal?")
            // Create a new node and update all of the wires connected to the endPoint connection join to be represented by this new node. 
            let newNode = Node()
            // All devices connected to this node also need that terminals node to be updated
            let connectedNodes = graph.floodFill(self.endPoint)
            
            if let safeNode = self.node {
                for element in connectedNodes {
                    if element.node(terminal: Terminal.Positive) == safeNode {
                        element.setNode(terminal: Terminal.Positive, node: newNode)
                    } else if element.node(terminal: Terminal.Negative) == safeNode {
                        element.setNode(terminal: Terminal.Negative, node: newNode)
                    } else {
                        assert(false, "How can the device be connected to the wire submap but not share a common node?")
                    }
                    // I don't like this solution to determine if the new node network is grounded, but I can't think of a more creative way to solve the problem of grounding when wires are removed.
                    // Cover case for node being connected to ground
                    if element.isKindOfClass(Ground) {
                        newNode.voltage = 0
                    }
                }
            } else {
                assert(false, "How can a wire not have a node?")
            }
            
            let originalNodes = graph.floodFill(self.startPoint)
            var foundGround = false
            for element in originalNodes {
                if element.isKindOfClass(Ground) {
                    foundGround = true
                }
            }
            if let safeNode = self.node {
                if foundGround {
                    // I'm not 100% confident this assertion will always hold.  It is safe to remove the assertion if issues arise. 
                    assert(safeNode.voltage! == 0, "If ground is already connected, the node should already have been set to 0V")
                    safeNode.voltage = 0 //Set the voltage to 0 just to be safe
                } else {
                    safeNode.voltage = nil // Something has happened, the voltage should go back to unsolved.
                }
            } else {
                assert(false, "Major logical error")
            }
            
        }
    }
    
}
