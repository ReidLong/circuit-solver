//
//  UINode.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/20/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

let NAME_KEY = "Name:"
let NODE_VOLTAGE_KEY = "Voltage:"

class UINode: UICircuitElement {
    
    var node: Node? = nil

    init(startPoint: CGPoint, endPoint: CGPoint, node: Node) {
        super.init(startPoint: startPoint, endPoint: endPoint)
        self.node = node
    }
    
    convenience init(startPoint: CGPoint, endPoint: CGPoint, node: Node, delegate: UICircuitElementDelegate) {
        self.init(startPoint: startPoint, endPoint: endPoint, node: node)
        self.delegate = delegate
        self.update()
    }
    override init(startPoint: CGPoint, endPoint: CGPoint) {
        super.init(startPoint: startPoint, endPoint: endPoint)
        self.node = Node() // Create a dummy node for an floating wire
    }
    
    
    override func generateInspectorData() -> [InspectorData] {
        var dataList = super.generateInspectorData()
        
        if let safeNode = self.node {
            let nodeNameData = InspectorData(key: NAME_KEY, value: safeNode.safeName, type: DataInputType.Text)
            dataList.append(nodeNameData)
            
            let nodeVoltageData = InspectorData(key: NODE_VOLTAGE_KEY, value: safeNode.voltageString, type: DataInputType.Static)
            dataList.append(nodeVoltageData)
        } else {
            assert(false, "Node Null on inspection")
        }
        
        return dataList
    }
    
    override func updateModel(data: InspectorData) -> Bool {
        let alreadyResolved = super.updateModel(data)
        if alreadyResolved {
            return alreadyResolved // true
        }
        
        switch data.fieldName {
        case NAME_KEY:
            if let safeNode = self.node {
                safeNode.name = data.fieldValue
            } else {
                assert(false, "What the heck is happening to this node?")
            }
            return true
        default:
            // do nothing
            ();
        }
        
        return false
    }
    
}