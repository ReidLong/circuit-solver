//
//  CurvedWire.swift
//  Circuit Solver
//
//  Created by Reid Long on 1/2/15.
//  Copyright (c) 2015 Reid Long. All rights reserved.
//

import UIKit
// The curved wire behavior is based on a tightly coupled design choice of how the devices are rendered. If the design of the CircuitViewController is changed, this behavior may no longer be valid. 
// The endPoint must be updated for every movement of the user's finger.  If this occurs, the curved wire will plot correctly.
class CurvedWire: Wire {

    // MARK: Internal Properties
    // Used for custom curved rendering behavior
    private var path: CGMutablePathRef = CGPathCreateMutable()
    
    // Used for calculating effective hit box
    private var minPoint: CGPoint
    private var maxPoint: CGPoint
    
    override var startPoint: CGPoint {
        didSet {
            println("Clearing Path: \(self.path)")
            self.initalizePath()
        }
    }
    
    override var endPoint: CGPoint {
        didSet {
            CGPathAddLineToPoint(self.path, nil, self.endPoint.x, self.endPoint.y)
            self.updateRange(self.endPoint)
        }
    }
    
    private func initalizePath() {
        self.path = CGPathCreateMutable()
        CGPathMoveToPoint(self.path, nil, self.startPoint.x, self.startPoint.y)
        CGPathAddLineToPoint(self.path, nil, self.endPoint.x, self.endPoint.y)
        self.minPoint = self.startPoint
        self.maxPoint = self.startPoint
        self.updateRange(self.endPoint)
    }
    
    override init(startPoint: CGPoint, endPoint: CGPoint, wireDelegate: WireDelegate) {
        self.minPoint = startPoint
        self.maxPoint = startPoint
        super.init(startPoint: startPoint, endPoint: endPoint, wireDelegate: wireDelegate)
        self.initalizePath()
    }
    
    private func updateRange(point: CGPoint) {
        self.minPoint.x = min(self.minPoint.x, point.x)
        self.minPoint.y = min(self.minPoint.y, point.y)
        self.maxPoint.x = max(self.maxPoint.x, point.x)
        self.maxPoint.y = max(self.maxPoint.y, point.y)
    }
    
    override func hitbox() -> [CGPoint] {
        let topLeft = self.minPoint
        let topRight = CGPointMake(self.maxPoint.x, self.minPoint.y)
        let bottomRight = self.maxPoint
        let bottomLeft = CGPointMake(self.minPoint.x, self.maxPoint.y)
        return [topLeft, topRight, bottomRight, bottomLeft]
    }
    
    override func redraw() {
        let bezierPath = UIBezierPath(CGPath: self.path)
        UIColor.whiteColor().setStroke()
        bezierPath.stroke()
    }
}