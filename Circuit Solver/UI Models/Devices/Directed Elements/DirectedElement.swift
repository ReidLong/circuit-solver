//
//  DirectedElement.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/21/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

let POSITIVE_NODE_NAME_KEY = "Positive Node Name:"
let NEGATIVE_NODE_NAME_KEY = "Negative Node Name:"
let POSITIVE_NODE_VOLTAGE_KEY = "Positive Node Voltage:"
let NEGATIVE_NODE_VOLTAGE_KEY = "Negative Node Voltage:"

class DirectedElement: UICircuitElement {
    var positiveTerminalPoint: CGPoint {
        get {
            return self.startPoint
        }
        set {
            self.startPoint = newValue
        }
    }
    
    var negativeTerminalPoint: CGPoint {
        get {
            return self.endPoint
        }
        set {
            self.endPoint = newValue
        }
        
    }
    
    var positiveNode: Node? = nil
    var negativeNode: Node? = nil

    
    override func node(#terminal: Terminal) -> Node? {
        switch terminal {
        case Terminal.Positive:
            assert(self.positiveNode != nil, "Node should not be nil when node function is called")
            return self.positiveNode
        case Terminal.Negative:
            assert(self.negativeNode != nil, "Node should not be nil when node function is called")
            return self.negativeNode
        }
    }
    
    override func setNode(#terminal: Terminal, node: Node) {
        switch terminal {
        case Terminal.Positive:
            self.positiveNode = node
        case Terminal.Negative:
            self.negativeNode = node
        }
    }
    
    override func generateInspectorData() -> [InspectorData] {
        var dataList = super.generateInspectorData()
        
        if let safePositiveNode = self.positiveNode {
            let positiveNodeData = InspectorData(key: POSITIVE_NODE_NAME_KEY, value: safePositiveNode.safeName, type: DataInputType.Text)
            dataList.append(positiveNodeData)
            
            let positiveNodeVoltageData = InspectorData(key: POSITIVE_NODE_VOLTAGE_KEY, value: safePositiveNode.voltageString, type: DataInputType.Static)
            dataList.append(positiveNodeVoltageData)
        } else {
            // This should never occur since all devices should be configured with nodes on creation. 
            assert(false, "Positive Node Not Configured for DirectedElement")
        }
        if let safeNegativeNode = self.negativeNode {
            let negativeNodeData = InspectorData(key: NEGATIVE_NODE_NAME_KEY, value: safeNegativeNode.safeName, type: DataInputType.Text)
            dataList.append(negativeNodeData)
            
            let negativeNodeVoltageData = InspectorData(key: NEGATIVE_NODE_VOLTAGE_KEY, value: safeNegativeNode.voltageString, type: DataInputType.Static)
            
            dataList.append(negativeNodeVoltageData)
        }
        
        
        return dataList
    }
    
    override func updateModel(data: InspectorData) -> Bool {
        let alreadyResolved = super.updateModel(data)
        if alreadyResolved {
            return alreadyResolved //true
        }
        switch data.fieldName {
        case POSITIVE_NODE_NAME_KEY:
            if var safePositiveNode = self.positiveNode {
                safePositiveNode.name = data.fieldValue
                return true
            } else {
                assert(false, "Positive Node Not Set")
            }
        case NEGATIVE_NODE_NAME_KEY:
            if var safeNegativeNode = self.negativeNode {
                safeNegativeNode.name = data.fieldValue
                return true
            } else {
                assert(false, "Negative Node Not Set")
            }
        default:
            // Do nothing 
            ();
            
        }
        return false
        
    }
    
}
