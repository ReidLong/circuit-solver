//
//  UIDevice+Removal.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/30/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

extension UIDevice {
    func processRemoval() {
        self.processRemoval(self.positiveNode)
        self.processRemoval(self.negativeNode)
    }
    
    private func processRemoval(node: Node?) {
        if let safeNode = node {
            safeNode.devices.remove(self.device)
        }
    }
}
