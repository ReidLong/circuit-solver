//
//  UIDevice.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/18/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

let DEVICE_NAME_KEY = "Name:"

class UIDevice: DirectedElement {
    
    var device: Device
    
    override var positiveNode: Node? {
        get {
            return self.device.positiveNode
        }
        set {
            if let safeNewNode = newValue {
                self.device.positiveNode = safeNewNode
            } else {
                assert(false, "New Value is Nil")
            }
        }
    }
    
    override var negativeNode: Node? {
        get {
            return self.device.negativeNode
        }
        set {
            if let safeNewNode = newValue {
                self.device.negativeNode = safeNewNode
            } else {
                assert(false, "New value is nil")
            }
        }
    }

    
    init(positiveTerminalPoint: CGPoint, negativeTerminalPoint: CGPoint, device: Device) {
        self.device = device
        super.init(startPoint: positiveTerminalPoint, endPoint: negativeTerminalPoint)
    }
    
    convenience init(positiveTerminalPoint: CGPoint, negativeTerminalPoint: CGPoint, device: Device, delegate: UICircuitElementDelegate) {
        self.init(positiveTerminalPoint: positiveTerminalPoint, negativeTerminalPoint: negativeTerminalPoint, device: device)
        self.delegate = delegate
        self.update()
    }
    
    override func generateInspectorData() -> [InspectorData] {
        var dataList = super.generateInspectorData()
        
        let deviceNameData = InspectorData(key: DEVICE_NAME_KEY, value: self.device.safeName, type: DataInputType.Text)
        dataList.insert(deviceNameData, atIndex: 0)
        
        return dataList
        
    }
    
    override func updateModel(data: InspectorData) -> Bool {
        let alreadyResolved = super.updateModel(data)
        if alreadyResolved {
            return alreadyResolved // true
        }
        switch data.fieldName {
        case DEVICE_NAME_KEY:
            self.device.name = data.fieldValue
            return true
        default:
            // do nothing
            ();
        }
        return false
    }
    
    
    
}
