//
//  UIResistor.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/18/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

let RESISTOR_MAX_HANDLE_LENGHT = CGFloat(30) //points
let RESISTOR_HANDLE_LENGTH_PERCENT: CGFloat = 0.15 // 15%
let LAMBA: CGFloat = 10 //points
let AMPLITUDE: CGFloat = 1.1 * LAMBA
let DELTA_X_MAGNITUDE: CGFloat = LAMBA / 4.0

let RESISTANCE_VALUE_KEY = "Resistance:"


class UIResistor: UIDevice {
    
    var resistor: Resistor {
        get {
            return self.device as Resistor
        }
    }
    
    init(positiveTerminalPoint: CGPoint, negativeTerminalPoint: CGPoint, resistor: Resistor) {
        super.init(positiveTerminalPoint: positiveTerminalPoint, negativeTerminalPoint: negativeTerminalPoint, device: resistor)
        
    }
    
    override var width: CGFloat {
        get {
            return AMPLITUDE * 2
        }
    }
    
    override func redraw() {
        var path = UIBezierPath()
        
        var offsetVector = Vector(magnitude: 1, angle: CGPoint.angle(self.positiveTerminalPoint, self.negativeTerminalPoint))
        //println("Vector Offset: \(offsetVector.description)")
        
        let directionNormalizer = self.negativeTerminalPoint.x - self.positiveTerminalPoint.x
        var directionMultiplier = copysign(1, directionNormalizer)
        if self.endPoint.x - self.startPoint.x == 0 {
            directionMultiplier = -1
        }
        
        let deltaX = DELTA_X_MAGNITUDE * directionMultiplier
        
        path.moveToPoint(self.positiveTerminalPoint)
        //Draw Left Handle
        let handleLength = self.calculateHandleLength()
        
        offsetVector.magnitude = handleLength
        let leftHandleEndPoint = CGPointMake(path.currentPoint.x + offsetVector.deltaX * directionMultiplier, path.currentPoint.y + offsetVector.deltaY * directionMultiplier)
        path.addLineToPoint(leftHandleEndPoint)
        
        //Draw Middle Squiggle
        let deviceLength = self.length
        let frequency = (deviceLength - 2 * handleLength) / LAMBA
        
        if frequency > 1 {
            //Draw First Offset 
            var nextPoint = UIResistor.CGPointMakeWithOffsets(path.currentPoint, angleVector: offsetVector, xMagnitude: deltaX, yMagnitude: AMPLITUDE)
            path.addLineToPoint(nextPoint)
            
            var isHigh = true
            
            let numberOfWaves = Int(frequency) * 2
           // println("Drawing \(numberOfWaves) resistor waves")
            //Draw Full Shape
            //Execute numberOfWaves - 1 times
            for _ in 1..<numberOfWaves {
                let deltaY = (isHigh ? -2 : 2) * AMPLITUDE
                nextPoint = UIResistor.CGPointMakeWithOffsets(path.currentPoint, angleVector: offsetVector, xMagnitude: 2 * deltaX, yMagnitude: deltaY)
                path.addLineToPoint(nextPoint)
                isHigh = !isHigh
                
            }
            //Draw Tail Offset
            
            nextPoint = UIResistor.CGPointMakeWithOffsets(path.currentPoint, angleVector: offsetVector, xMagnitude: deltaX, yMagnitude: AMPLITUDE)
            path.addLineToPoint(nextPoint)
        }
        
        //Draw Right Handle
        
        //offsetVector.magnitude = handleLength
        //let rightHandleEndPoint = CGPointMake(self.endPoint.x - offsetVector.deltaX, self.endPoint.y + offsetVector.deltaY)
        //path.addLineToPoint(rightHandleEndPoint)
        
        path.addLineToPoint(CGPointMake(self.negativeTerminalPoint.x, self.negativeTerminalPoint.y))
        path.lineWidth = 1
        UIColor.yellowColor().setStroke()
        path.stroke()
        //println("Path: \(path.description)")
    }
    
    class func CGPointMakeWithOffset(originalPoint: CGPoint, vector offset: Vector) -> CGPoint {
        
        let newX = originalPoint.x + offset.deltaX
        let newY = originalPoint.y + offset.deltaY
        
        return CGPointMake(newX, newY)
    }
    class func CGPointMakeWithInverseOffset(originalPoint: CGPoint, vector offset: Vector) -> CGPoint {
        let newX = originalPoint.x + offset.deltaY
        let newY = originalPoint.y - offset.deltaX
        
        return CGPointMake(newX, newY)
    }
    
    class func CGPointMakeWithOffsets(originalPoint: CGPoint, var angleVector offset: Vector, xMagnitude: CGFloat, yMagnitude: CGFloat) -> CGPoint {
        offset.magnitude = xMagnitude
        var output = UIResistor.CGPointMakeWithOffset(originalPoint, vector: offset)
        offset.magnitude = yMagnitude
        output = UIResistor.CGPointMakeWithInverseOffset(output, vector: offset)
        
        return output
    }
    
    
    func calculateHandleLength() -> CGFloat {
        let dynamicLength = RESISTOR_HANDLE_LENGTH_PERCENT * self.length
        return min(RESISTOR_MAX_HANDLE_LENGHT, max(MINIMUM_HANDLE_LENGTH, dynamicLength))
    }
    
    override func generateInspectorData() -> [InspectorData] {
        var dataList = super.generateInspectorData()
        
        let resistorData = InspectorData(key: RESISTANCE_VALUE_KEY, value: "\(self.resistor.resistance)\(RESISTANCE_UNIT)", type: DataInputType.Number(MIN_RESISTANCE, MAX_RESISTANCE))
        dataList.append(resistorData)
        
        
        return dataList
    }
    
    override func updateModel(data: InspectorData) -> Bool {
        let alreadyResolved = super.updateModel(data)
        if alreadyResolved {
            return alreadyResolved //true
        }
        switch data.fieldName {
        case RESISTANCE_VALUE_KEY:
            if let newValue = data.fieldValue.toFloat() {
                self.resistor.resistance = Double(newValue)
                return true
            } else {
                assert(false, "Invalid Data Input")
            }
        default:
            // do nothing 
            ();
            
        }
        return false
    }
    
}
