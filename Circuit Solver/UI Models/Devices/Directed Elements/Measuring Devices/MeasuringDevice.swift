//
//  MeasuringDevice.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/21/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

class MeasuringDevice: DirectedElement {
    func calculateOutput(circuit: Circuit) -> String? {
        assert(false, "Measuring Device is Abstract")
        return nil
    }
    
    func updateCalculations(circuit: Circuit) {
        assert(false, "Measuring Device is Abstract")
    }
}
