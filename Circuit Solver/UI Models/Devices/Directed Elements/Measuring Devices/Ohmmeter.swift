//
//  Ohmmeter.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/21/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

let MEASURING_DEVICE_CIRCLE_RADIUS: CGFloat = 15
let MEASURING_DEVICE_CIRCLE_DIAMETER: CGFloat = 2 * MEASURING_DEVICE_CIRCLE_RADIUS
let OHMMETER_SYMBOL: NSString = "Ω"

let OHMMETER_NAME_KEY = "Device Type:"
let EQUIVALENT_RESISTANCE_KEY = "Equivalent Resistance:"

class Ohmmeter: MeasuringDevice {
    
    var lastEquivalentResistance: Double? = nil
    
    var formattedEquivalentResistance: String {
        get {
            if let resistance = self.lastEquivalentResistance {
                let output =  "\(resistance)\(OHMMETER_SYMBOL)"
                println(output)
                return output
            }
            //No connected circuit should result in infinite resistance
            assert(false, "No Equivalent Resistance Calculated")
            return "<Error>"
        }
    }
    
    init(startPoint: CGPoint, endPoint: CGPoint, nodeA: Node, nodeB: Node) {
        super.init(startPoint: startPoint, endPoint: endPoint)
        self.positiveNode = nodeA
        self.negativeNode = nodeB
    }
    
    override init(startPoint: CGPoint, endPoint: CGPoint) {
        super.init(startPoint: startPoint, endPoint: endPoint)
    }
    
    override var width: CGFloat {
        get {
            // No buffer applied currently
            return MEASURING_DEVICE_CIRCLE_DIAMETER
        }
    }
    
    override func redraw() {
        self.drawSymbolOutline()
        self.drawLabel()
        
    }
    
    
    func drawLabel() {
        
        //OHMMETER_SYMBOL.drawAtPoint(center, withAttributes: self.labelFontAttributes())
        
        let boundingRect = OHMMETER_SYMBOL.boundingRectWithSize(CGSizeMake(MEASURING_DEVICE_CIRCLE_DIAMETER, MEASURING_DEVICE_CIRCLE_DIAMETER), options: NSStringDrawingOptions.UsesDeviceMetrics, attributes: self.labelFontAttributes(), context: NSStringDrawingContext())
        let center = self.center
        let rect = CGRectMake(center.x - boundingRect.size.width / 2, center.y - boundingRect.size.height / 2, boundingRect.size.width, boundingRect.size.height)
        
        OHMMETER_SYMBOL.drawInRect(rect, withAttributes: self.labelFontAttributes())
        
    }
    
    func labelFontAttributes() -> [NSObject: AnyObject] {
        let font = UIFont(name: "Helvetica Bold", size: 14)
        var textStyle = NSMutableParagraphStyle.defaultParagraphStyle().mutableCopy() as NSMutableParagraphStyle
        textStyle.alignment = NSTextAlignment.Center
        
        return [NSFontAttributeName: font!, NSForegroundColorAttributeName: UIColor.whiteColor(), NSParagraphStyleAttributeName: textStyle]
    }
    
    func drawSymbolOutline() {
        var path = UIBezierPath()
        
        var offsetVector = Vector.unitVector(atan((self.startPoint.y - self.endPoint.y) / (self.startPoint.x - self.endPoint.x)))
        var directionMultiplier = copysign(1, self.endPoint.x - self.startPoint.x)
        if self.endPoint.x - self.startPoint.x == 0 {
            directionMultiplier = -1
        }
        println("Angle: \(offsetVector.angle) Direction Multiplier: \(directionMultiplier)")
        println("StartPoint: \(self.startPoint) EndPoint: \(self.endPoint)")
        
        path.moveToPoint(self.startPoint)
        
        let handleLength = self.calculateHandleLength()
        
        println("Handle Length: \(handleLength)")
        
        offsetVector.magnitude = handleLength
        
        let leftHandleEndPoint = CGPointMake(path.currentPoint.x + offsetVector.deltaX * directionMultiplier, path.currentPoint.y + offsetVector.deltaY * directionMultiplier)
        
        path.addLineToPoint(leftHandleEndPoint)
        
        let center = CGPoint.center(self.startPoint, self.endPoint)
        println("Center: \(center)")
        let arcStartPoint = CGPointMake(center.x + MEASURING_DEVICE_CIRCLE_RADIUS, center.y)
        path.moveToPoint(arcStartPoint)
        
        path.addArcWithCenter(center, radius: MEASURING_DEVICE_CIRCLE_RADIUS, startAngle: 0, endAngle: CGFloat(2*π), clockwise: true)
        
        path.moveToPoint(self.endPoint)
        
        offsetVector.magnitude = -handleLength
        let rightHandleEndPoint = CGPointMake(path.currentPoint.x + offsetVector.deltaX * directionMultiplier, path.currentPoint.y + offsetVector.deltaY * directionMultiplier)
        path.addLineToPoint(rightHandleEndPoint)
        
        path.lineWidth = 1
        UIColor.blueColor().setStroke()
        path.stroke()
        println("Path: \(path.description)")
    }
    
    func calculateHandleLength() -> CGFloat {
        return (self.length - 2 * MEASURING_DEVICE_CIRCLE_RADIUS) / 2
    }
    
    func isConnected(circuit: Circuit) -> Bool {
        if let nodeA = self.positiveNode {
            if let nodeB = self.negativeNode {
                return circuit.nodes.contains(nodeA) && circuit.nodes.contains(nodeB)
            }
        }
        return false
    }
    
    
    override func calculateOutput(circuit: Circuit) -> String? {
        self.updateCalculations(circuit)
        return "Equivalent Resistance: \(self.formattedEquivalentResistance)"
    }
    
    override func updateCalculations(circuit: Circuit) {
        if self.isConnected(circuit) {
            // If the circuit is connected, it should imply that the nodes are valid for safe unwraping
            let resistance = EquivalentResistanceSolver(circuit: circuit, nodeA: self.positiveNode!, nodeB: self.negativeNode!).solve()
            self.lastEquivalentResistance = resistance
        }
        println("Circuit not connected")
    }
    
    
    override func generateInspectorData() -> [InspectorData] {
        var dataList = super.generateInspectorData()
        
        // Hardcoded indices should be safe
        let deviceNameData = InspectorData(key: OHMMETER_NAME_KEY, value: "Ohmmeter", type: DataInputType.Static)
        dataList.insert(deviceNameData, atIndex: 0)
        
        let equivalentResistanceData = InspectorData(key: EQUIVALENT_RESISTANCE_KEY, value: self.formattedEquivalentResistance, type: DataInputType.Static)
        dataList.insert(equivalentResistanceData, atIndex: 1)
        
        return dataList
    }
    
}
