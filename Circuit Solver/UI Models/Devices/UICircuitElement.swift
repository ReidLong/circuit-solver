//
//  UICircuitElement.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/20/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

let MINIMUM_HANDLE_LENGTH: CGFloat = 5 //points
let MINIMUM_CIRCUIT_ELEMENT_LENGTH = MINIMUM_HANDLE_LENGTH * 2 //points

let HIT_DETECTION_BUFFER: CGFloat = 10 //points radius

protocol UICircuitElementDelegate {
    func redrawNeeded()
}

class UICircuitElement: NSObject {
    
    var delegate: UICircuitElementDelegate? = nil {
        didSet{
            self.update()
        }
    }
    
    
    var startPoint: CGPoint {
        didSet {
            self.update()
        }
    }
    
    var endPoint: CGPoint {
        didSet {
            self.update()
        }
    }
    
    var center: CGPoint {
        return CGPoint.center(self.startPoint, self.endPoint)
    }
    var length: CGFloat {
        get {
            return hypot(self.startPoint.x - self.endPoint.x, self.startPoint.y - self.endPoint.y)
        }
    }
    
    var width: CGFloat {
        get {
            return 2 * HIT_DETECTION_BUFFER
        }
    }
    
    var deleted: Bool = false

    
    init(startPoint: CGPoint, endPoint: CGPoint) {
        self.startPoint = startPoint
        self.endPoint = endPoint
        super.init()
    }

    
    func update() {
        if let safeDelegate = self.delegate {
            safeDelegate.redrawNeeded()
        }
    }
    
    func draw() {
        self.drawHitbox() // Only used for diagnostics to ensure all hit boxes are working properly
        
        if self.length >= MINIMUM_CIRCUIT_ELEMENT_LENGTH {
            redraw()
        } else {
            //Draw as wire
            println("Device too small to render")
            drawWire()
        }
    }
    
    func drawWire() {
        var path = UIBezierPath()
        path.moveToPoint(self.startPoint)
        path.addLineToPoint(self.endPoint)
        path.lineWidth = 1
        UIColor.whiteColor().setStroke()
        path.stroke()
        
    }

    class func makeHitbox(pointA: CGPoint, _ pointB: CGPoint, width: CGFloat) -> [CGPoint] {
        // The magnitude probably should have been self.width / 2 however, the behavior is pretty good with the full width so I'm leaving it for now
        let vector = Vector(magnitude: width, angle: CGPoint.angle(pointA, pointB))
        
        var pointList = pointA.offset(vector)
        let tempList = pointB.offset(vector)
        
        // Investigate ways to do this dynamically.  Hardcoding is scary
        pointList.insert(tempList[0], atIndex: 1)
        pointList.insert(tempList[1], atIndex: 2)
        
        return pointList

    }
    
    
    //Output order is significant
    func hitbox() -> [CGPoint] {
        return UICircuitElement.makeHitbox(self.startPoint, self.endPoint, width: self.width)
    }
    
    // Debug function
    func drawHitbox() {
        let points = self.hitbox()
        var path = UIBezierPath()
        path.moveToPoint(points[0])
        for point in points {
            path.addLineToPoint(point)
        }
        path.addLineToPoint(points[0])
        UIColor.redColor().setStroke()
        path.stroke()
    }
    
    
    
    func didCollide(point: CGPoint) -> Bool {
        
        let hitbox = self.hitbox()
        
        return CollisionDetection.hasCollision(hitbox, testPoint: point)
    }
    
    
    /*
        The class generating the data should also implement the coresponding update functionality for keys producted by the generate function.

    */
    
    // Overriding subclasses must start data chain with the super collection
    func generateInspectorData() -> [InspectorData] {
        var data: [InspectorData] = []
        
        // No keys implemented by UICircuitElement
        
        return data
    }
    
    // Overriding subclasses must call super class to ensure all neccessary updates are carried up the chain
    // Return true when the data has been updated (optimization)
    func updateModel(data: InspectorData) -> Bool {
        
        if !data.dataType.supportsEditing() {
            return true //Prevent all data updates from being executed if the field is not executable
        }
        
        
        // No keys implemented by UICircuitElement
        
        return false
    }
    
    func node(terminal _: Terminal) -> Node? {
        assert(false, "Abstract CircuitElement")
        return nil
    }
    
    func setNode(terminal _: Terminal, node: Node) {
        assert(false, "Abstract CircuitElement")
    }
    
    func point(#terminal: Terminal) -> CGPoint {
        switch terminal {
        case Terminal.Positive:
            return self.startPoint
        case Terminal.Negative:
            return self.endPoint
        }
    }
    
    func setPoint(#terminal: Terminal, point: CGPoint) {
        switch terminal {
        case Terminal.Positive:
            self.startPoint = point
        case Terminal.Negative:
            self.endPoint = point
        }
    }
    
    func canConnect(terminal _: Terminal) -> Bool {
        // Defaults to connectable at all terminals 
        return true
    }
    
    // Abstract
    // Classes implementing redraw must also override the width property
    func redraw() {
        
        assert(false, "Draw Not Implemented For CircuitElement")
    }
    
    override var description: String {
        get {
            return "Circuit Element: \(self.startPoint), \(self.endPoint)"
        }
    }
}