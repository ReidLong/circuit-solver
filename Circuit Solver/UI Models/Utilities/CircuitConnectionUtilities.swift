//
//  CircuitConnectionUtilities.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/28/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import UIKit

class CircuitConnectionUtilities {
    
    private init () {
        // private initalizer used to prevent the instantiation of this class 
        assert(false)
    }
    
    class func snap(element keyElement: UICircuitElement, toList testElements: [UICircuitElement], direction keyTerminal: Terminal) {
        assert(keyElement.canConnect(terminal: keyTerminal), "Device Not Compatible")
        let keyPoint = keyElement.point(terminal: keyTerminal)
        for testElement in testElements {
            if testElement.canConnect(terminal: Terminal.Positive) && CircuitConnectionUtilities.connectDevices(testElement, templateDeviceTerminal: Terminal.Positive, newDevice: keyElement, newDeviceTerminal: keyTerminal) {
                // Terminal Connected on the positive end
                break // Don't let a double connection form at the same terminal
            } else if testElement.canConnect(terminal: Terminal.Negative) && CircuitConnectionUtilities.connectDevices(testElement, templateDeviceTerminal: Terminal.Negative, newDevice: keyElement, newDeviceTerminal: keyTerminal) {
                // Terminal Connected on the negative end
                break // Don't let a double connection form at the same terminal
            }
        }
    }
    
    private class func connectDevices(templateDevice: UICircuitElement, templateDeviceTerminal: Terminal, newDevice: UICircuitElement, newDeviceTerminal: Terminal) -> Bool {
        let templateDevicePoint = templateDevice.point(terminal: templateDeviceTerminal)
        let newDevicePoint = newDevice.point(terminal: newDeviceTerminal)
        
        if templateDevicePoint ~== newDevicePoint {
            // Connection is valid
            newDevice.setPoint(terminal: newDeviceTerminal, point: templateDevicePoint) // Visible Snap Behavior
            
            // Process the node
            let node = templateDevice.node(terminal: templateDeviceTerminal)
            assert(node != nil, "Possibly need to handle the case where the device cannot actually be connected to this terminal")
            newDevice.setNode(terminal: newDeviceTerminal, node: node!)
            return true
        }
        return false
    }
}
