//
//  InspectorData.swift
//  Circuit Solver
//
//  Created by Reid Long on 12/21/14.
//  Copyright (c) 2014 Reid Long. All rights reserved.
//

import Foundation

enum DataInputType {
    case Static
    case Text
    case Number(Int, Int) //Min, Max
    
    func supportsEditing() -> Bool {
        switch self {
        case .Static:
            return false
        case let .Number(minValue, maxValue):
            fallthrough
        case .Text:
            return true
        }

    }
    
}

struct InspectorData {
    let fieldName: String
    var fieldValue: String
    var dataType: DataInputType
    init(key: String, value: String, type: DataInputType) {
        self.fieldName = key
        self.fieldValue = value
        self.dataType = type
    }
}